package com.ibibl.isabel.processor;

public class SimpleTokenizerProcessorTest {

	public static void main (String [] args){
		
		String text = "For GOd so loved the world, that he \"gave his one and only son\" gave his one and only son, that "
				+ "whoever believes in Him shouhld not perish, but have eternal life";
		
		Processor proc = new SimpleTokenizerProcessor();
		String [] tokens = proc.getTokens(text);
		for (String token : tokens){
			System.out.println(token);
		}
	}
}
