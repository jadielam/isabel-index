package com.ibibl.isabel.ranking;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.Collections;
import java.util.List;
import java.util.Random;

import com.google.common.math.DoubleMath;

public class SortingPerformanceTest {

	public static void main(String[] args){
		
		List<Long> timeTaken = new LinkedList<Long>();
		for (int i = 0; i < 50; ++i){
			
			Random random = new Random(System.currentTimeMillis());
			List<Integer> values = new ArrayList<Integer>();
			for (int j = 0; j < 3000; ++j){
				int a = random.nextInt();	
				values.add(a);
			}
			long a = System.currentTimeMillis();
			Collections.sort(values);
			long b = System.currentTimeMillis();
			timeTaken.add(b-a);
			
		}
		
		double average = DoubleMath.mean(timeTaken);
		System.out.println(average);
		
	}
}
