package com.ibibl.isabel.ranking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Random;
import java.util.Set;

import com.google.common.math.DoubleMath;
import com.ibibl.isabel.common.PaginationResults;
import com.ibibl.isabel.index.FlattenedInvertedIndex;
import com.ibibl.isabel.index.IndexAccessor;
import com.ibibl.isabel.index.InvertedIndex;
import com.ibibl.isabel.index.Pair;

public class PerformanceTest {

	public static void main(String[] args) {
		
		//1. Generate a collection of 40 000 documents
		String characters = "qwertyuiopasdfghjklzxcvbnm1234568790";
		Random random = new Random(System.currentTimeMillis());
		
		Set<String> words = new HashSet<String>();
		Set<Pair<Integer, String>> collection = new HashSet<Pair<Integer, String>>();
		for (int i = 0 ; i < 40000; ++i){
			
			StringBuilder document = new StringBuilder();
			for (int j = 0 ; j < 40; j++){
				StringBuilder word = new StringBuilder();
				for (int k = 0; k < 10; ++k){
					int index = random.nextInt(characters.length());
					word.append(characters.charAt(index));
				}
				words.add(word.toString());
				document.append(word).append(" ");
			}
			
			Pair<Integer, String> doc = new Pair<Integer, String>(i, document.toString());
			collection.add(doc);
		}
		//1.1 Keep track of the words used to generate the collection.
		
		//2. Use the words to generate 50 queries
		InvertedIndex index = new FlattenedInvertedIndex();
		//index.addCollection("test", collection);
		List<String> wordsList = new ArrayList<String>(words);
		List<String> queries = new ArrayList<String>();
		for (int i = 0; i < 50; ++i){
			StringBuilder query = new StringBuilder();
			
			for (int j = 0; j < 1; j++){
				int nextIndex = random.nextInt(wordsList.size());
				query.append(wordsList.get(nextIndex));
				query.append(" ");
			}
			queries.add(query.toString());
		}
		
		//3. Measure the time it takes to execute the 50 queries
		//Ranker ranker = new SimpleVerseRanker();
		List<Long> times = new ArrayList<>(queries.size());
		for (int i = 0 ; i < queries.size(); ++i){
			String query = queries.get(i);
			//IndexAccessor accessor = index.getIndexAccessor("test");
			
			long a = System.currentTimeMillis();
			//PaginationResults results = ranker.rank(accessor, new HashMap<Integer, Integer>(), query, 0, 20, "test");
			long b = System.currentTimeMillis();
			long time = b - a;
			times.add(time);
			
			
		}
		
		//4. Print the average time it took to finish each of the 50 queries
		double average = DoubleMath.mean(times);
		System.out.println(average);

	}
	
	
}
