package com.ibibl.isabel.ranking;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.ibibl.isabel.index.TrieNode;
import com.ibibl.isabel.processor.Processor;
import com.ibibl.isabel.processor.SimpleTokenizerProcessor;
import com.ibibl.isabel.index.VerseEntry;
import com.ibibl.isabel.index.BibleVerse;

public class PhraseRankerTest {

	private Processor processor;
	private Ranker ranker;
	private TrieNode index;
	
	public PhraseRankerTest(){
		this.processor = new SimpleTokenizerProcessor();
		this.ranker = new PhraseRanker();
		this.index = new TrieNode();
		
	}
	
	public static void main(String[] args) {
		PhraseRankerTest test = new PhraseRankerTest();
		test.buildIndex();
		test.getRankedBibleVerses("This is love");
		

	}
	
	public void getRankedBibleVerses(String query){	
		List<BibleVerse> bibleVerses = new ArrayList<BibleVerse>();
		
		String [] tokens = this.processor.getTokens(query);
		//LOGGER.info("List of tokens of size "+tokens.length);
		//long [] documentIds = this.ranker.rank(this.index, tokens, 20, "NIV");
		
	}
	private Collection<BibleVerse> buildCollection(){
		
		Collection<BibleVerse> toReturn = new ArrayList<BibleVerse>();
		BibleVerse verse1 = new BibleVerse();
		verse1.setBook("John");
		verse1.setChapter(3);
		verse1.setId(1L);
		verse1.setText("For God so loved the world, that he gave his one and only son, that whoever believes in him should not perish but have eternal life");
		verse1.setVerse(16);
		//toReturn.add(verse1);
		
		BibleVerse verse2 = new BibleVerse();
		verse2.setBook("Gen");
		verse2.setChapter(1);
		verse2.setId(2L);
		verse2.setText("In the beginning God created the heavens and the earth. And the earth was voidless and formless");
		verse2.setVerse(1);
		//toReturn.add(verse2);
		

		BibleVerse verse3 = new BibleVerse();
		verse3.setBook("2Th");
		verse3.setChapter(1);
		verse3.setId(3L);
		verse3.setText("We ought always to thank God for you, brothers, and rightly so, because your faith is growing more and more, and the love every one of you has for each other is increasing.");
		verse3.setVerse(3);
		//toReturn.add(verse3);
		
		BibleVerse verse4 = new BibleVerse();
		verse4.setBook("1Ti");
		verse4.setChapter(1);
		verse4.setId(4L);
		verse4.setText("The goal of this command is love, which comes from a pure heart and a good conscience and a sincere faith");
		verse4.setVerse(5);
		toReturn.add(verse4);
		
		return toReturn;
	}
	
    private void buildIndex(){
		
		Collection<BibleVerse> allVerses = this.buildCollection();
		
		for (BibleVerse bibleVerse : allVerses){
			String verseText = bibleVerse.getText();
			String [] tokens = this.processor.getTokens(verseText);
			Map<String, com.ibibl.isabel.index.VerseEntry> wordPositionsMap = new HashMap<String, com.ibibl.isabel.index.VerseEntry>();
			
			for (int i = 0; i < tokens.length; ++i){
				String word = tokens[i];
				if (wordPositionsMap.containsKey(word)){
					wordPositionsMap.get(word).positions.add(i);
				}
				else{
					List<Integer> positions = new ArrayList<Integer>();
					positions.add(i);
					VerseEntry verseEntry = new VerseEntry(bibleVerse.getId(), 
							"NIV", positions);
					
					wordPositionsMap.put(word, verseEntry);
				}
			}
			
			Set<Entry<String, VerseEntry>> entrySet = wordPositionsMap.entrySet();
			for (Entry<String, VerseEntry> entry : entrySet){
				String word = entry.getKey();
				VerseEntry verseEntry = entry.getValue();
				List<Integer> positions = verseEntry.positions;
				
				int [] positionsArray = new int[positions.size()];
				
				int i=0;
				for (Integer a : positions){
					positionsArray[i]=a;
					i++;
				}
				long a = verseEntry.documentId;
				this.index.insert(word, verseEntry.documentId, positionsArray, 
						verseEntry.collection);
			}
			
		}
		
	}
	
}
