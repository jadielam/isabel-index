package com.ibibl.isabel.ranking;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.ibibl.isabel.common.PaginationResults;
import com.ibibl.isabel.index.TrieNode;
import com.ibibl.isabel.processor.Processor;
import com.ibibl.isabel.index.FlattenedInvertedIndex;
import com.ibibl.isabel.index.IndexAccessor;
import com.ibibl.isabel.index.InvertedIndex;
import com.ibibl.isabel.index.Pair;
import com.ibibl.isabel.index.VerseEntry;
import com.ibibl.isabel.index.BibleVerse;

public class PhraseRankerTest1{

	private Ranker ranker;
	private InvertedIndex index;
	
	public PhraseRankerTest1(){
		
		this.ranker = new PhraseRanker();
		this.index = new FlattenedInvertedIndex();
		
	}
	
	public static void main(String[] args) {
		PhraseRankerTest1 test = new PhraseRankerTest1();
		test.buildIndex();
		test.getRankedBibleVerses("For God so loved the world that he gave his one and only son, that whoever believes in him should not perish but have eternal life. For god did not send his son into the world.");
		

	}
	
	public PaginationResults getRankedBibleVerses(String query){	
				
		//LOGGER.info("List of tokens of size "+tokens.length);
		IndexAccessor accessor = this.index.getIndexAccessor("NIV");
		PaginationResults documentIds = this.ranker.rank(accessor, new HashMap<Integer, Integer>(), query, 0, 20, "NIV");
		return documentIds;
		
		
	}
	private Collection<BibleVerse> buildCollection(){
		
		Collection<BibleVerse> toReturn = new ArrayList<BibleVerse>();
		BibleVerse verse1 = new BibleVerse();
		verse1.setBook("John");
		verse1.setChapter(3);
		verse1.setId(1L);
		verse1.setText("For God so loved the world, that he gave his one and only son, that whoever believes in him should not perish but have eternal life");
		verse1.setVerse(16);
		//toReturn.add(verse1);
		
		BibleVerse verse2 = new BibleVerse();
		verse2.setBook("Gen");
		verse2.setChapter(1);
		verse2.setId(2L);
		verse2.setText("In the beginning God created the heavens and the earth. And the earth was voidless and formless");
		verse2.setVerse(1);
		//toReturn.add(verse2);
		

		BibleVerse verse3 = new BibleVerse();
		verse3.setBook("2Th");
		verse3.setChapter(1);
		verse3.setId(3L);
		verse3.setText("We ought always to thank God for you, brothers, and rightly so, because your faith is growing more and more, and the love every one of you has for each other is increasing.");
		verse3.setVerse(3);
		//toReturn.add(verse3);
		
		BibleVerse verse4 = new BibleVerse();
		verse4.setBook("1Ti");
		verse4.setChapter(1);
		verse4.setId(4L);
		verse4.setText("The goal of this command is love, which comes from a pure heart and a good conscience and a sincere faith");
		verse4.setVerse(5);
		toReturn.add(verse4);
		
		return toReturn;
	}
	
    private void buildIndex(){
		
		Collection<BibleVerse> allVerses = this.buildCollection();
		
		Set<Pair<Integer, String>> collection = new HashSet<Pair<Integer, String>>();
		
		for (BibleVerse bibleVerse : allVerses){
			String verseText = bibleVerse.getText();
			Long longId = bibleVerse.getId();
			Integer id = longId.intValue();
			collection.add(new Pair<Integer, String>(id, verseText));
		}
		
		this.index.addCollection("NIV", collection);
	}
	
}
