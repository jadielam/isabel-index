package com.ibibl.isabel.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.ibibl.isabel.processor.Processor;
import com.ibibl.isabel.processor.SimpleTokenizerProcessor;
import com.ibibl.isabel.ranking.Ranker;
import com.ibibl.isabel.ranking.SimpleVerseRanker;


public class IndexTest {

	public static void main(String [] args){
	
		//. Entries to add to the index.
		List<Pair<Long, String>> entries = new ArrayList<Pair<Long, String>>();
		Pair<Long, String> pair1 = new Pair<Long, String>(2L, "This");
		//Pair<Long, String> pair2 = new Pair<Long, String>(2L, "The string number 2.");
		entries.add(pair1);
		//entries.add(pair2);
		
		//. Creating the index
		TrieNode index = new TrieNode();
		
		Processor processor = new SimpleTokenizerProcessor();
		
		for (Pair<Long, String> entri : entries){
			String verseText = entri.getSecond();
			String [] tokens = processor.getTokens(verseText);
			Map<String, VerseEntry> wordPositionsMap = new HashMap<String, VerseEntry>();
			
			for (int i = 0; i < tokens.length; ++i){
				String word = tokens[i];
				if (wordPositionsMap.containsKey(word)){
					wordPositionsMap.get(word).positions.add(i);
				}
				else{
					List<Integer> positions = new ArrayList<Integer>();
					positions.add(i);
					VerseEntry verseEntry = new VerseEntry(entri.getFirst(), 
							"NIV", positions);
					
					wordPositionsMap.put(word, verseEntry);
				}
			}
			
			Set<Entry<String, VerseEntry>> entrySet = wordPositionsMap.entrySet();
			for (Entry<String, VerseEntry> entry : entrySet){
				String word = entry.getKey();
				VerseEntry verseEntry = entry.getValue();
				List<Integer> positions = verseEntry.positions;
				
				int [] positionsArray = new int[positions.size()];
				
				int i=0;
				for (Integer a : positions){
					positionsArray[i]=a;
					i++;
				}
				long a = verseEntry.documentId;
				index.insert(word, verseEntry.documentId, positionsArray, 
						verseEntry.collection);
			}
	
		}	
		
		System.out.print("Insertion code works well");
		System.out.print("There are some memory optimizations that can be made.");
		
		String [] query = {"This"};
		
		
	}
}
