package com.ibibl.isabel.index;

import java.util.List;

public class VerseEntry{
	
	public Long documentId;
	
	public String collection;
	
	public List<Integer> positions;
	
	public VerseEntry(Long documentId, String collection, List<Integer> positions){
		this.documentId = documentId;
		this.collection = collection;
		this.positions = positions;
	}
}

