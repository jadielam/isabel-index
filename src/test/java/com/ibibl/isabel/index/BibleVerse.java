package com.ibibl.isabel.index;


public class BibleVerse {

	private Long id;
	
	private String text;
	
	private String book;
	
	private Integer chapter;
	
	private Integer verse;
	
	public BibleVerse(){
		
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

	

	public String getBook() {
		return book;
	}

	public void setBook(String book) {
		this.book = book;
	}

	public Integer getChapter() {
		return chapter;
	}

	public void setChapter(Integer chapter) {
		this.chapter = chapter;
	}

	public Integer getVerse() {
		return verse;
	}

	public void setVerse(Integer verse) {
		this.verse = verse;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((book == null) ? 0 : book.hashCode());
		result = prime * result + ((chapter == null) ? 0 : chapter.hashCode());
		result = prime * result + ((verse == null) ? 0 : verse.hashCode());
	
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		BibleVerse other = (BibleVerse) obj;
		if (book == null) {
			if (other.book != null)
				return false;
		} else if (!book.equals(other.book))
			return false;
		if (chapter == null) {
			if (other.chapter != null)
				return false;
		} else if (!chapter.equals(other.chapter))
			return false;
		if (verse == null) {
			if (other.verse != null)
				return false;
		} else if (!verse.equals(other.verse))
			return false;
		return true;
	}

	public String toString(){
		StringBuilder sb = new StringBuilder();
		sb.append(this.book);
		sb.append(" ");
		sb.append(this.chapter.toString());
		sb.append(":");
		sb.append(this.verse.toString());
		sb.append(" ");
		sb.append(this.text);
		return sb.toString();
	}
	
}
