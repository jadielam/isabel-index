package com.ibibl.isabel.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;

public class TrieTest {

	public static void main(String[] args) {
		
		String letters = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ";
		Trie trie = new Trie();
		HashMap<String, String> map = new HashMap<String, String>();
		List<String> stringsInserted = new ArrayList<String>();
		
		for (int i = 0; i < 50000; ++i){
			StringBuilder sb = new StringBuilder();
			Random random = new Random();
			for (int j = 0; j < 10; j++){
				sb.append(letters.charAt(random.nextInt(letters.length()-1)));
			}
			stringsInserted.add(sb.toString());
			
		}
		
		long a = System.currentTimeMillis();
		for (int i = 0; i < stringsInserted.size(); ++i){
			map.put(stringsInserted.get(i), null);
		}
		long b = System.currentTimeMillis();
		System.out.println(b-a);
		
		a = System.currentTimeMillis();
		for (int i = 0; i < stringsInserted.size(); ++i){
			trie.insert(stringsInserted.get(i));
		}
		
		b = System.currentTimeMillis();
		System.out.println(b-a);
		
		
		a = System.currentTimeMillis();
		for (int i = 0; i < stringsInserted.size(); ++i){
			trie.getWordId((stringsInserted.get(i)));
		}
		b = System.currentTimeMillis();
		System.out.println(b-a);
		
		a = System.currentTimeMillis();
		for (int i = 0; i < stringsInserted.size(); ++i){
			map.get(stringsInserted.get(i));
		}
		
		b = System.currentTimeMillis();
		System.out.println(b-a);
		
		
	}
}