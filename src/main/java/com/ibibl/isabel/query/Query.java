package com.ibibl.isabel.query;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import com.ibibl.isabel.index.Field;

public class Query {

	private final String text;
	private final Set<Field> fields;
	
	public Query(String text, Set<Field> fields){
		this.text = text;
		this.fields = new HashSet<Field>(fields);
	}

	/**
	 * Returns the text of the query
	 * @return
	 */
	public String getText() {
		return text;
	}

	/**
	 * Returns the fields where we are to search
	 * @return
	 */
	public Set<Field> getFields() {
		return Collections.unmodifiableSet(this.fields);
	}
}
