package com.ibibl.isabel.ranking;

import java.util.Map;

import com.ibibl.isabel.common.PaginationResults;
import com.ibibl.isabel.index.IndexAccessor;

public interface OldRanker {

	PaginationResults rank(IndexAccessor accessor,
			Map<Integer, Integer> freqMap, String query, Integer start,
			Integer noDocuments, String collection);

}
