package com.ibibl.isabel.ranking;


import java.util.Map;

import com.ibibl.isabel.common.PaginationResults;
import com.ibibl.isabel.index.Field;
import com.ibibl.isabel.index.IndexAccessor;
import com.ibibl.isabel.query.Query;


public interface Ranker {

	
	/**
	 * Returns a list of ranked documents from index for that given query.
	 * @param accessor
	 * @param freqMap
	 * @param query
	 * @param start
	 * @param noDocuments
	 * @param fields
	 * @return
	 */
	public PaginationResults rank(IndexAccessor accessor, 
			Map<Integer, Integer> freqMap, 
			Query query, 
			Integer start, 
			Integer noDocuments);
	
}
