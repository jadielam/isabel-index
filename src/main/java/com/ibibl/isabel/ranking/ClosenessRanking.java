package com.ibibl.isabel.ranking;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NavigableMap;
import java.util.NavigableSet;
import java.util.Set;
import java.util.TreeMap;

import com.ibibl.isabel.index.DocumentEntry;
import com.ibibl.isabel.index.IndexAccessor;
import com.ibibl.isabel.index.TrieNode;
import com.ibibl.isabel.search.Searcher;
import com.ibibl.isabel.search.SimpleSearch;

//TODO: Finish this class an engineer everything else.
public class ClosenessRanking implements Ranker {

	private Searcher searcher;

	public ClosenessRanking(){
		this.searcher = new SimpleSearch();
	}
	
	public long [] rank(TrieNode index, String[] query, int noDocuments, String collection) {
		
		Map<Long, Float> rankings = new HashMap<Long, Float>();
		Map<Long, RankingEntry> rankingEntries = new HashMap<>();
		
		//1. Gathering data for each document
		for (String word : query){
			TrieNode node = searcher.search(index, word);
			
			if (null != node){
				List<DocumentEntry> entries = node.getDocumentEntries(collection);
				for (DocumentEntry entry : entries){
					long documentId = entry.getDocId();
					int[] positions = entry.getPositions();
					RankingEntry rEntry;
					if (rankingEntries.containsKey(documentId)){
						rEntry = rankingEntries.get(documentId);
					}
					else{
						rEntry = new RankingEntry(documentId);
						rankingEntries.put(documentId, rEntry);
					}
										
					for (int position : positions){
						rEntry.addPosition(position);
					}
					rankingEntries.put(documentId, rEntry);
				}
			}
		}
		
		//Calculating the ranking value of each document
		Set<Entry<Long, RankingEntry>> entries = rankingEntries.entrySet();
		int queryLength = query.length;
		for (Entry<Long, RankingEntry> e : entries){
			long docId = e.getKey();
			RankingEntry rEntry = e.getValue();
			
			int max = rEntry.getMaximumPosition();
			int min = rEntry.getMinimumPosition();
			int numberOfWords = rEntry.getNumberOfWords();
			float ranking = (queryLength/(Math.max(max-min+1, queryLength)+0.5f) * numberOfWords);
			
			rankings.put(docId, ranking);
		}
		
		//Sorting ranking values.
		//TODO: Do it in a faster way.
		TreeMap<Float, List<Long>> sortedValues = new TreeMap<Float, List<Long>>();
		for (Entry<Long, Float> entry : rankings.entrySet()){
			long docId = entry.getKey();
			float rank = entry.getValue();
			
			if (sortedValues.containsKey(rank)){
				sortedValues.get(rank).add(docId);
			}
			else{
				List<Long> documents = new ArrayList<Long>();
				documents.add(docId);
				sortedValues.put(rank, documents);
			}
			
		}
		
		//Returning back the top k documents
		int toReturnSize = Math.min(noDocuments, rankings.size());
		long [] toReturn = new long[toReturnSize];
		NavigableSet<Float> orderedKeys = sortedValues.descendingKeySet();
		
		int i = 0;
		boolean finishit = false;
		for (Float key : orderedKeys){
			List<Long> docs = sortedValues.get(key);
			for (Long docId : docs){
				if (i >= toReturnSize){
					finishit = true;
					break;
				}
				toReturn[i]=docId;
				++i;
			}	
			if (finishit) break;
		}
		
		return toReturn;
		
	}



	@Override
	public long[] rank(IndexAccessor accessor, String query, int noDocuments,
			String collection) {
		//TODO
		throw new UnsupportedOperationException();
		
	}

}


