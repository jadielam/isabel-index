package com.ibibl.isabel.ranking;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.google.common.collect.ImmutableList;
import com.ibibl.isabel.common.PaginationResults;
import com.ibibl.isabel.index.Document;
import com.ibibl.isabel.index.DocumentEntry;
import com.ibibl.isabel.index.DocumentTerms;
import com.ibibl.isabel.index.DocumentTermsCollection;
import com.ibibl.isabel.index.IndexAccessor;
import com.ibibl.isabel.index.Pair;
import com.ibibl.isabel.index.TrieNode;
import com.ibibl.isabel.processor.Processor;
import com.ibibl.isabel.search.Searcher;
import com.ibibl.isabel.search.SimpleSearch;

public class PhraseRanker implements Ranker {

	private static final int DEFAULT_NUMBER_OF_RESULTS = 20;
	
	public PhraseRanker(){
		
	}
	
	@Override
	public PaginationResults rank(IndexAccessor accessor, Map<Integer, Integer> freqMap, String query, Integer start, Integer k,
			String collection) {
		
		int startIntValue = 0;
		if (null != start){
			startIntValue = start.intValue();
		}
		
		int kIntValue = DEFAULT_NUMBER_OF_RESULTS;
		if (null != k){
			kIntValue = k.intValue();
		}

		
		query = query.trim();
		if (query.length() == 0) return new PaginationResults(0, startIntValue, new int[0]);
		
		//1. Process the query and convert it to tokens
		Processor processor = accessor.getProcessor();
		String [] tokens = processor.getTokens(query);
		List<Integer> tokenIds =  new ArrayList<Integer>();
		
		//2. Gathering data for each document in the query
		for (String token : tokens){
			int id = accessor.getTermId(token);
		
			//TODO: Check if not adding this token here
			//is correct and proper.
			if (id != -1) tokenIds.add(id);
		}
		
		//the key is the doc id.  The value is a list with the positions for
		//words that are present in this query. It is an ordered list.
		Set<Integer> tokenIdsSet = new HashSet<>(tokenIds);
		Map<Integer, List<Integer>> docsPositions = new HashMap<>();
		for (Integer tokenId : tokenIdsSet){
			
			DocumentTermsCollection documentTermsCol = accessor.getDocumentTermsCollection(tokenId);
			for (DocumentTerms docTerms : documentTermsCol){
				int docId = docTerms.getDocumentId();
				List<Integer> positions;
				if (docsPositions.containsKey(docId)){
					positions = docsPositions.get(docId);
				}
				else{
					positions = new ArrayList<>(2*tokens.length);
					docsPositions.put(docId, positions);
				}
				for (int i = 0; i < docTerms.size(); i++){
					positions.add(docTerms.get(i));
				}
				
				
			}
		}
		
		
		//3. Ranking each of the documents to the given query.
		
		//Contains the ranking for each relevant document
		List<Pair<Integer, Float>> rankings = new ArrayList<Pair<Integer, Float>>(docsPositions.size());
		
		
		Set<Entry<Integer, List<Integer>>> entrySet = docsPositions.entrySet();
		for (Entry<Integer, List<Integer>> e : entrySet){
			Integer docId = e.getKey();
			List<Integer> wordPositions = e.getValue();
			Collections.sort(wordPositions);
			
			float rank = getDocumentRank(accessor, collection, docId, wordPositions, 
					tokenIds, tokenIdsSet);
			rankings.add(new Pair<Integer, Float>(docId, rank));
		}
		
		//2.3 Return the top k documents.
		Collections.sort(rankings, (p1, p2) -> Float.compare(p1.getSecond(), p2.getSecond())); //Note that we are doing reverse ordering here.

		int startPlace = Math.max(-1, rankings.size() - 1 - startIntValue);
		int returnSize = Math.min(kIntValue, startPlace + 1);
		int stopPlace = Math.max(-1, startPlace - returnSize);
		int [] toReturn = new int[returnSize];
		int counter = 0;
		for (int i = startPlace; i > stopPlace; --i){
			toReturn[counter] = rankings.get(i).getFirst();
			counter++;
		}
		
		return new PaginationResults(rankings.size(), startIntValue, toReturn);

	}
	
	/**
	 * 
	 * @param accessor
	 * @param collection
	 * @param docId
	 * @param wordPositions contains a list of positions of the words from the query in the original
	 * document.
	 * @param queryTokenIds
	 * @param queryTokenIdsSet
	 * @return
	 */
	private float getDocumentRank(IndexAccessor accessor, String collection, int docId, 
			List<Integer> wordPositions, List<Integer> queryTokenIds, Set<Integer> queryTokenIdsSet){
		
		if (wordPositions.size()<1) return 0f;
		
		int queryLength = queryTokenIds.size();
		int maxRank = 0;
		Document doc = accessor.getDocument(docId);
		
		int [] delimiters = new int[2];
		delimiters[0] = -1;
		delimiters[1] = -1;
		int previous_delimiter = -1;
	
		do{
			//1. Find the next delimiters
			previous_delimiter = delimiters[1];
			delimiters = this.getNextWindowDelimiters(delimiters, queryLength, wordPositions);
			if (null == delimiters) break;
			
			//2. Find the rank of the next delimiters
			Set<Integer> queryWordsPresent = new HashSet<Integer>();
			int a = wordPositions.get(delimiters[0]);
			int b = wordPositions.get(delimiters[1]);
			
			
			for (int i = a; i <= b; ++i){
				Integer wordId = doc.getWordIdAtPosition(i);
				
				if (queryTokenIdsSet.contains(wordId) && !queryWordsPresent.contains(wordId)){
					queryWordsPresent.add(wordId);
				}
			}
			int rank = queryWordsPresent.size();
			
			if (rank > maxRank) maxRank = rank;

		}
		while (previous_delimiter != delimiters[1]);
			
		return maxRank;
	}
	

	/**
	 * Windows delimiters are the inclusive position indices of a Document.
	 * @param previousDelimiters
	 * @param queryLength
	 * @param docsPositions
	 * @return the window delimiters.  The window delimiters are positions in the original document that
	 * contain a window of size at most the size of the query, with words from the query.
	 */
	private int [] getNextWindowDelimiters(int [] previousDelimiters, int queryLength, List<Integer> docsPositions){
		int [] toReturn = new int[2];
		int a_p = previousDelimiters[0];
		int b_p = previousDelimiters[1];
		int docsPositions_size = docsPositions.size();
		
		if ((docsPositions_size - 1 ) < ( a_p + 1)
				|| (docsPositions_size -1) < (b_p + 1) 
				|| b_p < a_p){
			return null;
		}
		
		int a_n = a_p + 1;
		int position_a_n = docsPositions.get(a_n);
		int b_n = b_p + 1;
		int position_b_n = docsPositions.get(b_n);
		while (a_n < docsPositions_size-1){
			
			boolean loopEntered = false;
			while (b_n < docsPositions_size-1
					&& position_b_n < position_a_n + queryLength){
				loopEntered = true;
				b_n++;
				position_b_n = docsPositions.get(b_n);
			}
			
			if (loopEntered){
				if (position_b_n < position_a_n + queryLength){
					toReturn[0] = a_n;
					toReturn[1] = b_n;
					return toReturn;
				}
				else{
					b_n--;
					toReturn[0] = a_n;
					toReturn[1] = b_n;
					return toReturn;
				}
				
			}
			
			if (b_n > docsPositions_size-1) return null;
			a_n++;
			position_a_n = docsPositions.get(a_n);
		}
		return null;
	}
	
	private int [] dumb(int [] previousDelimiters, int queryLength, List<Integer> docsPositions){
		
		return null;
	}
	

}


