package com.ibibl.isabel.ranking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

/**
 * For each document it contains the word positions for each of the words
 * from a query that are also in the document
 * @author jadiel
 *
 */
public class RankingEntry {
	
	/**
	 * doc id that this entry represents
	 */
	private final long docId;
	
	/**
	 * the positions of words from the query in the document
	 */
	private final List<Integer> wordPositions;
	
		
	public RankingEntry(long docId){
		this.docId = docId;
		this.wordPositions = new ArrayList<Integer>(10);
	}
	
	public void addPosition(int position){
		this.wordPositions.add(position);
	}
	
	public int getMinimumPosition(){
		return Collections.min(wordPositions);
	}
	
	public int getMaximumPosition(){
		return Collections.max(wordPositions);
	}
	
	public int getNumberOfWords(){
		return this.wordPositions.size();
	}
	
	public long getDocId(){
		return this.docId;
	}
	
	
}
