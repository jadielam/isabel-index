package com.ibibl.isabel.ranking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import com.google.common.collect.ImmutableList;
import com.ibibl.isabel.index.Pair;

public class RankingPhraseEntry {

	/**
	 * doc id that this entry represents
	 */
	private final long docId;
	
	private final List<Pair<Integer, String>> wordPositions;
	
	public RankingPhraseEntry(long docId){
		this.docId = docId;
		this.wordPositions = new ArrayList<Pair<Integer, String>>();
	}
	
	public void addWordPosition(int position, String word){
		Pair<Integer, String> positionWord = new Pair<>(position, word);
		this.wordPositions.add(positionWord);
	}
	
	public long getDocId(){
		return this.docId;
	}
	
	public List<Pair<Integer, String>> getWordPositions(){
		return this.wordPositions;
	}
}


class OrderedRankingPhraseEntry {

	private final long docId;
	
	private final ImmutableList<Pair<Integer, String>> wordPositions;
	
	public OrderedRankingPhraseEntry(long docId, List<Pair<Integer, String>> wordPositions){
		this.docId = docId;
		Collections.sort(wordPositions, new PairComparator());
		this.wordPositions = ImmutableList.copyOf(wordPositions);
				
	}
	
	public OrderedRankingPhraseEntry(RankingPhraseEntry  phraseEntry){
		this(phraseEntry.getDocId(), phraseEntry.getWordPositions());
	}
	
	public long getDocId(){ 
		return docId;
	}
	
	public ImmutableList<Pair<Integer, String>> getWordPositions(){
		return this.wordPositions;
	}
}

class PairComparator implements Comparator<Pair<Integer, String>>{

	@Override
	public int compare(Pair<Integer, String> o1, Pair<Integer, String> o2) {
		
		return Integer.compare(o1.getFirst(), o2.getFirst());
	}
	
}
