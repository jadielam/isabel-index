package com.ibibl.isabel.ranking;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.Map.Entry;

import com.ibibl.isabel.common.PaginationResults;
import com.ibibl.isabel.index.Document;
import com.ibibl.isabel.index.DocumentTerms;
import com.ibibl.isabel.index.DocumentTermsCollection;
import com.ibibl.isabel.index.Field;
import com.ibibl.isabel.index.FieldAccessor;
import com.ibibl.isabel.index.IndexAccessor;
import com.ibibl.isabel.index.Pair;
import com.ibibl.isabel.processor.Processor;
import com.ibibl.isabel.query.Query;

public class PhraseFrequencyRanker implements Ranker {

	private static final int DEFAULT_NUMBER_OF_RESULTS = 20;
	
	@Override
	public PaginationResults rank(IndexAccessor accessor, Map<Integer, Integer> freqMap, 
			Query query, Integer start, Integer k) {
		
		int startIntValue = 0;
		if (null != start){
			startIntValue = start.intValue();
		}
		
		int kIntValue = DEFAULT_NUMBER_OF_RESULTS;
		if (null != k){
			kIntValue = k.intValue();
		}
		if (null == query) return new PaginationResults(0, startIntValue, new int[0]);
		String queryText = query.getText();
		if (null == queryText) return new PaginationResults(0, startIntValue, new int[0]);
		queryText = queryText.trim();
		if (queryText.length() == 0) return new PaginationResults(0, startIntValue, new int[0]);
		
		//The different fields where the ranker needs to look out.
		Set<Field> fields = query.getFields();
		
		//Contains the processed query text for the processor
		Map<Processor, String []> processedQueries = new HashMap<Processor, String []>();
		
		//Contains the ranking score for each document.
		Map<Integer, Float> documentRanks = new HashMap<Integer, Float>();
		
		for (Field field : fields){
			FieldAccessor fieldAccessor = accessor.getFieldAccessor(field);
			Processor processor = fieldAccessor.getProcessor();
			String [] tokens = null;
			if (!processedQueries.containsKey(processor)){
				tokens = processor.getTokens(queryText);
				processedQueries.put(processor, tokens);
			}
			else{
				tokens = processedQueries.get(processor);
			}
			
			//Gathering data for each token in the query.
			List<Integer> tokenIds = new ArrayList<Integer>();
			for (String token : tokens){
				int id = accessor.getTermId(token);
				
				if (id != -1) tokenIds.add(id);
			}
			
			Set<Integer> tokenIdsSet = new HashSet<>(tokenIds);

			//the key is the doc id.  The value is a list with the positions for
			//words that are present in this query. It is an ordered list.
			Map<Integer, List<Integer>> docsPositions = new HashMap<>();
		
			//Contains the idf value for each word in the query.
			Map<Integer, Double> wordsIdf = new HashMap<Integer, Double>();
			int numberOfDocuments = fieldAccessor.getNoDocuments();
			
			for (Integer tokenId : tokenIdsSet){
				DocumentTermsCollection documentTermsCol = fieldAccessor.getDocumentTermsCollection(tokenId);
				
				//Number of documents where this term appears
				int termDocumentCount = documentTermsCol.size();
				double idf = 1.0 + Math.log(numberOfDocuments/(termDocumentCount + 1.0));
				wordsIdf.put(tokenId, idf);
				
				for (DocumentTerms docTerms : documentTermsCol){
					int docId = docTerms.getDocumentId();
					List<Integer> positions;
					if (docsPositions.containsKey(docId)){
						positions = docsPositions.get(docId);
					}
					else{
						positions = new ArrayList<>(2*tokens.length);
						docsPositions.put(docId, positions);
					}
					for (int i = 0; i < docTerms.size(); i++){
						positions.add(docTerms.get(i));
					}
				}
			}
			
			Set<Entry<Integer, List<Integer>>> entrySet = docsPositions.entrySet();
			for (Entry<Integer, List<Integer>> e : entrySet){
				Integer docId = e.getKey();
				List<Integer> wordPositions = e.getValue();
				Collections.sort(wordPositions);
				
				float rank = getDocumentRank(fieldAccessor, docId, wordPositions, 
						tokenIds, tokenIdsSet, wordsIdf);
				
				if (documentRanks.containsKey(docId)){
					float previousRank = documentRanks.get(docId);
					if (rank > previousRank){
						documentRanks.put(docId, rank);
					}
				}else{
					documentRanks.put(docId,rank);
				}
			}

		}
		
	
		//3. Ranking each of the documents to the given query.
		
		//Contains the ranking for each relevant document
		List<Pair<Integer, Float>> rankings = new ArrayList<Pair<Integer, Float>>(documentRanks.size());
		Set<Entry<Integer, Float>> ranksEntrySet = documentRanks.entrySet();
		for (Entry<Integer, Float> entry : ranksEntrySet){
			rankings.add(new Pair<Integer, Float>(entry.getKey(), entry.getValue()));
		}
		
		Comparator<Pair<Integer, Float>> comparator = new FrequencyTieComparator(freqMap);
		//2.3 Return the top k documents.
		Collections.sort(rankings, comparator); //Note that we are doing reverse ordering here.
		
		int startPlace = Math.max(-1, rankings.size() - 1 - startIntValue);
		int returnSize = Math.min(kIntValue, startPlace + 1);
		int stopPlace = Math.max(-1, startPlace - returnSize);
		int [] toReturn = new int[returnSize];
		int counter = 0;
		for (int i = startPlace; i > stopPlace; --i){
			toReturn[counter] = rankings.get(i).getFirst();
			counter++;
		}

		return new PaginationResults(rankings.size(), startIntValue, toReturn);
	}
	
	/**
	 * Solves ties in the ranking by comparing the frequency of the usage of the verb.
	 * @author jadiel
	 *
	 */
	class FrequencyTieComparator implements Comparator<Pair<Integer, Float>>{

		private final Map<Integer, Integer> freqMap;
		
		public FrequencyTieComparator(Map<Integer, Integer> freqMap){
			this.freqMap = freqMap;
		}
		
		@Override
		public int compare(Pair<Integer, Float> p1, Pair<Integer, Float> p2) {
			
			Float f1 = p1.getSecond();
			Float f2 = p2.getSecond();
			
			int tempReturn = Float.compare(f1, f2);
			if (tempReturn != 0) return tempReturn;
			
			Integer id1 = p1.getFirst();
			Integer id2 = p2.getFirst();
			
			Integer freq1 = this.freqMap.get(id1);
			Integer freq2 = this.freqMap.get(id2);
			
			if (null != freq1 && null != freq2){
				return Integer.compare(freq1, freq2);
			}
			return 0;
		}
		
	}
	
	/**
	 * 
	 * @param accessor
	 * @param collection
	 * @param docId
	 * @param wordPositions contains a list of positions of the words from the query in the original
	 * document.
	 * @param queryTokenIds
	 * @param queryTokenIdsSet
	 * @return
	 */
	private float getDocumentRank(FieldAccessor accessor, int docId, 
			List<Integer> wordPositions, List<Integer> queryTokenIds, Set<Integer> queryTokenIdsSet,
			Map<Integer, Double> queryTokensIdf){
		
		if (wordPositions.size()<1) return 0f;
		
		int queryLength = queryTokenIds.size();
		float maxRank = 0;
		Document doc = accessor.getDocument(docId);
		
		int [] delimiters = new int[2];
		delimiters[0] = -1;
		delimiters[1] = -1;
		int previous_delimiter = -1;
	
		do{
			//1. Find the next delimiters
			previous_delimiter = delimiters[1];
			delimiters = this.getNextWindowDelimiters(delimiters, queryLength, wordPositions);
			if (null == delimiters) break;
			
			//2. Find the rank of the next delimiters
			Set<Integer> queryWordsPresent = new HashSet<Integer>();
			int a = wordPositions.get(delimiters[0]);
			int b = wordPositions.get(delimiters[1]);
			
			
			for (int i = a; i <= b; ++i){
				Integer wordId = doc.getWordIdAtPosition(i);
				
				if (queryTokenIdsSet.contains(wordId) && !queryWordsPresent.contains(wordId)){
					queryWordsPresent.add(wordId);
				}
			}
			float rank = 0;
			for (Integer wordId : queryWordsPresent){
				double word_idf = queryTokensIdf.get(wordId);
				rank += word_idf;
			}
			
			if (rank > maxRank) maxRank = rank;

		}
		while (previous_delimiter != delimiters[1]);
			
		return maxRank;
	}
	

	/**
	 * Windows delimiters are the inclusive position indices of a Document.
	 * @param previousDelimiters
	 * @param queryLength
	 * @param docsPositions
	 * @return the window delimiters.  The window delimiters are positions in the original document that
	 * contain a window of size at most the size of the query, with words from the query.
	 */
	private int [] getNextWindowDelimiters(int [] previousDelimiters, int queryLength, List<Integer> docsPositions){
		int [] toReturn = new int[2];
		int a_p = previousDelimiters[0];
		int b_p = previousDelimiters[1];
		int docsPositions_size = docsPositions.size();
		
		if ((docsPositions_size - 1 ) < ( a_p + 1)
				|| (docsPositions_size -1) < (b_p + 1) 
				|| b_p < a_p){
			return null;
		}
		
		int a_n = a_p + 1;
		int position_a_n = docsPositions.get(a_n);
		int b_n = b_p + 1;
		int position_b_n = docsPositions.get(b_n);
		while (a_n < docsPositions_size-1){
			
			boolean loopEntered = false;
			while (b_n < docsPositions_size-1
					&& position_b_n < position_a_n + queryLength){
				loopEntered = true;
				b_n++;
				position_b_n = docsPositions.get(b_n);
			}
			
			if (loopEntered){
				if (position_b_n < position_a_n + queryLength){
					toReturn[0] = a_n;
					toReturn[1] = b_n;
					return toReturn;
				}
				else{
					b_n--;
					toReturn[0] = a_n;
					toReturn[1] = b_n;
					return toReturn;
				}
				
			}
			
			if (b_n > docsPositions_size-1) return null;
			a_n++;
			position_a_n = docsPositions.get(a_n);
		}
		return null;
	}

}

