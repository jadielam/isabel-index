package com.ibibl.isabel.ranking;

import java.util.Map;

import com.ibibl.isabel.common.PaginationResults;
import com.ibibl.isabel.index.IndexAccessor;

public class SimpleVerseRanker implements OldRanker {

	//private static final int DEFAULT_NUMBER_OF_RESULTS = 20;
	
	
	public SimpleVerseRanker(){
	
	}
	
	@Override
	public PaginationResults rank(IndexAccessor accessor, Map<Integer, Integer> freqMap, String query, Integer start, Integer noDocuments,
			String collection) {
		throw new UnsupportedOperationException();
		
		/**
		int startIntValue = 0;
		if (null != start){
			startIntValue = start.intValue();
		}
		
		int kIntValue = DEFAULT_NUMBER_OF_RESULTS;
		if (null != noDocuments){
			kIntValue = noDocuments.intValue();
		}

		query = query.trim();
		//TODO: Fix here
		if (query.length() == 0) return new PaginationResults(0, startIntValue,new int[0]);
		
		//1. Process the query and convert it to tokens
		Processor processor = accessor.getProcessor();
		String [] tokens = processor.getTokens(query);
		List<Integer> tokenIds = new ArrayList<Integer>();
		
		for (String token : tokens){
			int id = accessor.getTermId(token);
			
			if (id != -1) tokenIds.add(id);
		}

		ArrayList<Set<Integer>> sortedDocs = new ArrayList<Set<Integer>>(tokens.length);
		for (int i = 0; i < tokens.length; ++i){
			sortedDocs.add(new HashSet<Integer>());
		}

		//the key is the docid. The valu is a list with the positions for
		//words that are present in this query.   It is an ordered list.
		Set<Integer> tokenIdsSet = new HashSet<>(tokenIds);
		Map<Integer, Integer> docWordCounts = new HashMap<>();
		
		for (Integer tokenId : tokenIdsSet){
			DocumentTermsCollection documentTermsCol = accessor.getDocumentTermsCollection(tokenId);
			for (DocumentTerms docTerms: documentTermsCol){
				int docId = docTerms.getDocumentId();
				Integer wordCountsForDoc = docWordCounts.get(docId);
				if (null != wordCountsForDoc){
					sortedDocs.get(wordCountsForDoc-1).remove(docId);
					sortedDocs.get(wordCountsForDoc).add(docId);
					docWordCounts.put(docId, wordCountsForDoc + 1);
				}
				else{
					sortedDocs.get(0).add(docId);
					docWordCounts.put(docId, 1);
				}		
			}
		}
		
		//3. Rank the documents (the rank is already done.
		
		if (noDocuments <= 0) return new PaginationResults(0, startIntValue, new int[0]);
		
		int [] rankedDocuments = new int[Math.min(docWordCounts.size(), noDocuments)];
		
		//TODO: Implement pagination here.
		int noDocs = 0;
		boolean breakLoop = false;
		for (int i = sortedDocs.size()-1; i>=0; --i){
			Set<Integer> documentIds = sortedDocs.get(i);
			for (Integer documentId : documentIds){
				rankedDocuments[noDocs]=documentId;
				noDocs++;
				if (noDocs >= noDocuments) {
					breakLoop = true;
					break;
				}
			}
			if (breakLoop) break;
		}
		
		return new PaginationResults(docWordCounts.size(), startIntValue, rankedDocuments);
		**/
	}
	
	
	
}
