package com.ibibl.isabel.processor;

public class SimpleTokenizerProcessor implements Processor{

	private final String TYPE = SimpleTokenizerProcessor.class.getName();
	
	@Override
	public String[] getTokens(String text) {
		String [] tokens = text.split("[\\p{Punct}\\s]+");
		for (int i = 0; i < tokens.length; ++i){
			tokens[i] = tokens[i].toLowerCase();
		}
		return tokens;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((TYPE == null) ? 0 : TYPE.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleTokenizerProcessor other = (SimpleTokenizerProcessor) obj;
		if (TYPE == null) {
			if (other.TYPE != null)
				return false;
		} else if (!TYPE.equals(other.TYPE))
			return false;
		return true;
	}
	
}
