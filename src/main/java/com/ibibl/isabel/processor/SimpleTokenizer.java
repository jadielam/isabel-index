package com.ibibl.isabel.processor;

import java.io.StringReader;
import java.util.List;

import edu.stanford.nlp.ling.CoreLabel;
import edu.stanford.nlp.process.CoreLabelTokenFactory;
import edu.stanford.nlp.process.PTBTokenizer;
import edu.stanford.nlp.process.TokenizerFactory;

public class SimpleTokenizer implements Processor {
	
	private TokenizerFactory<CoreLabel> tokenizerFactory;
	
	public SimpleTokenizer(){
		this.tokenizerFactory = PTBTokenizer.factory(new CoreLabelTokenFactory(), "");
		
	}
	@Override
	public String[] getTokens(String text) {
		List<CoreLabel> tokens = tokenizerFactory.getTokenizer(new StringReader(text)).tokenize();
		String [] wordsArray = new String[tokens.size()];
		
		int i = 0;
		for (CoreLabel token : tokens){
			String value = token.toString();
			value = value.toLowerCase();
			wordsArray[i]=value;
			++i;
		}
		return wordsArray;
	}

}
