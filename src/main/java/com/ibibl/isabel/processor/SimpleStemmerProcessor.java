package com.ibibl.isabel.processor;

public class SimpleStemmerProcessor implements Processor {

	private final String TYPE = SimpleStemmerProcessor.class.getName();
	
	@Override
	public String[] getTokens(String text) {
		
		throw new UnsupportedOperationException();
		
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((TYPE == null) ? 0 : TYPE.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		SimpleStemmerProcessor other = (SimpleStemmerProcessor) obj;
		if (TYPE == null) {
			if (other.TYPE != null)
				return false;
		} else if (!TYPE.equals(other.TYPE))
			return false;
		return true;
	}
	
	

}
