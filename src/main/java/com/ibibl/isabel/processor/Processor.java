package com.ibibl.isabel.processor;

public interface Processor {

	/**
	 * Takes a chunk of text (sentence or paragraph) and returns
	 * the tokens of the text, in the order that they appear in the 
	 * text.
	 * @param text
	 * @return
	 */
	public String [] getTokens(String text);
}
