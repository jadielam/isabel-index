// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.isabel.index;

/**
 * @author jdearmas
 * Memory efficient character-TrieNode pair class
 * @since 1.0
 */
public class TriePair {
	
	/**
	 * A character letter
	 */
	private char letter;
	
	/**
	 * A node from the trie
	 */
	private Trie node;
	
	/**
	 * Constructor that sets both values of this pair class
	 * @param aLetter the letter
	 * @param aNode the node
	 */
	public TriePair(char aLetter, Trie aNode){
		this.letter = aLetter;
		this.node = aNode;
	}
	
	/**
	 * Returns the letter
	 * @return the letter
	 */
	public char getLetter(){
		return this.letter;
	}
	
	/**
	 * Returns the node
	 * @return the node.
	 */
	public Trie getNode(){
		return this.node;
	}
}
