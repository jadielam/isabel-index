package com.ibibl.isabel.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

import com.ibibl.isabel.processor.Processor;
import com.ibibl.isabel.processor.SimpleTokenizer;

public class FixedInvertedIndex {

	/**
	 * The root node to the trie datastructure that contains all the words
	 * together with the id of that word.
	 */
	private final Map<String, Integer> wordIds;
	
	/**
	 * Contains the the inverted indexes for each of the collections
	 * The collections are identified by a name.
	 */
	private final Map<String, int []> invertedIndexes;
	
	/**
	 * For each collection, it contains a map from wordId to wordPosition in 
	 * the inverted index for that collection.
	 */
	private final Map<String, Map<Integer, Integer>> wordPositions;
	
	private int wordCount = 0;
	
	/**
	 * Processor used to tokenize the documents inserted to the index.
	 */
	private Processor processor;
	
	public FixedInvertedIndex(){
		this.wordIds = new HashMap<>();
		this.invertedIndexes = new HashMap<String, int[]>();
		this.wordPositions = new HashMap<String, Map<Integer, Integer>>();
		this.processor = new SimpleTokenizer();
	}
	
	/**
	 * Adds the collection to the index
	 * @param collectionName
	 * @param documents A list of pairs, with the integer being the documentId and the String being
	 * the text of that document
	 */
	public void addCollection(String collectionName, Set<Pair<Integer, String>> documents){
		
		//0. Temporary storage
		Map<String, List<Pair<Integer, List<Integer>>>> tempInvertedIndex = new HashMap<>();
		
		//1. Populate temporary inverted index
		for (Pair<Integer, String> document : documents){
			int docId = document.getFirst();
			String docText = document.getSecond();
			String [] tokens = processor.getTokens(docText);
			
			
			int positionCounter = 0;
			Map<String, List<Integer>> wordPositions = new HashMap<String, List<Integer>>();
			for (String token : tokens){
				
				if (wordPositions.containsKey(token)){
					List<Integer> positions = wordPositions.get(token);
					positions.add(positionCounter);
				}
				else {
					List<Integer> positions = new ArrayList<Integer>();
					positions.add(positionCounter);
					wordPositions.put(token, positions);
				}
				positionCounter++;
			}
			Set<Entry<String, List<Integer>>> entrySet = wordPositions.entrySet();
			for (Entry<String, List<Integer>> e : entrySet){
				String token = e.getKey();
				List<Integer> positions = e.getValue();
				Pair<Integer, List<Integer>> docPositions = new Pair<>(docId, positions);
				if (tempInvertedIndex.containsKey(token)){
					List<Pair<Integer, List<Integer>>> tempPositions = tempInvertedIndex.get(token);
					tempPositions.add(docPositions);
				}
				else{
					List<Pair<Integer, List<Integer>>> tempPositions = new ArrayList<Pair<Integer, List<Integer>>>();
					tempPositions.add(docPositions);
					tempInvertedIndex.put(token, tempPositions);
				}
			}
		}
		
		//2. Make actual inverted index out of temporary inverted index.
		//2.0 Initializations
		Map<Integer, Integer> wordid_to_wordposition = new HashMap<Integer, Integer>();
		
		//2.1 Calculate the size of the actual inverted index.
		int invertedIndexSize = 0;
		invertedIndexSize += 1; 	//Field with the number of words
		Set<Entry<String, List<Pair<Integer, List<Integer>>>>> entrySet = tempInvertedIndex.entrySet();
		for (Entry<String, List<Pair<Integer, List<Integer>>>> e : entrySet){
			invertedIndexSize += 1;		//wordId
			invertedIndexSize += 1;		//Field with number of documents
			List<Pair<Integer, List<Integer>>> documentsPositions = e.getValue();
			for (Pair<Integer, List<Integer>> document : documentsPositions){
				invertedIndexSize += 1;  //docId
				invertedIndexSize += 1; //numberOfPositions
				
				List<Integer> positions = document.getSecond();
				int numberOfPositions = positions.size();
				invertedIndexSize += numberOfPositions; 	//one entry for each position.
			}
		}
		
		//2.2 Allocate the int[] array for that number of positions and build the index.
		int [] index = new int[invertedIndexSize];
		
		//next position in the index
		int next = 0; 			
		//2.2.1 Add number of words
		index[next] = tempInvertedIndex.size();
		next++;
		for (Entry<String, List<Pair<Integer, List<Integer>>>> e : entrySet){
			String token = e.getKey();
			
			//Add word id
			int wordId;
			if (this.wordIds.containsKey(token)){
				wordId = this.wordIds.get(token);
			}
			else{
				wordId = this.wordCount;
				this.wordIds.put(token, wordId);
				this.wordCount++;
			}
			index[next] = wordId;
			wordid_to_wordposition.put(wordId, next);
			next++;
			
			//Add number of documents
			List<Pair<Integer, List<Integer>>> docs = e.getValue();
			int noDocs = docs.size();
			index[next] = noDocs;
			next++;
			
			//For each document
			for (Pair<Integer, List<Integer>> document : docs){
				//add doc id
				int docId = document.getFirst();
				index[next] = docId;
				next++;
				
				//add number of positions
				List<Integer> positions = document.getSecond();
				int noPositions = positions.size();
				index[next] = noPositions;
				next++;
				
				//add each position
				for (Integer i : positions){
					index[next] = i;
					next++;
				}
			}
		}
		
		this.invertedIndexes.put(collectionName, index);
		this.wordPositions.put(collectionName, wordid_to_wordposition);
	}
	
	/**
	 * Returns the top k ranked documents from collection for the given query.
	 * @param query
	 * @param collectionName
	 * @param k
	 * @return
	 */
	public int[] getBestRankedDocuments(String query, String collectionName, int k){
		return null;
	}
	
	
	
}
