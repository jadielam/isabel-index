package com.ibibl.isabel.index;

/**
 * This class is a representation of DocumentTerms in the 
 * inverted index.
 * 
 * The class is just a sequence of the terms of the document, 
 * where the terms are represented as integers.
 * 
 * The class does not implement the Iterable interface
 * because it deals with primitive datatypes, and I don't
 * want too much boxing and unboxing, because of GC overhead.
 * 
 * @author jadiel
 *
 */
public interface DocumentTerms {

	public abstract DocumentTermsIterator iterator();
	
	public abstract int get(int i);
	
	public abstract int size();
	
	public abstract int getDocumentId();
	
}
