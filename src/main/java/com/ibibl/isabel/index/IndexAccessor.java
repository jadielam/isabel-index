package com.ibibl.isabel.index;

import java.util.Set;

import com.ibibl.isabel.processor.Processor;

/**
 * The IndexAccessor accesses the internal datastructures of the index
 * in order to give to the user of it the DocumentTermsCollection for a 
 * given term.
 * @author jadiel
 *
 */
public abstract class IndexAccessor {

	public IndexAccessor(){

	}
	
	/**
	 * Returns the field accessor for the given field.
	 * @param field
	 * @return
	 */
	public abstract FieldAccessor getFieldAccessor(Field field);
	
	/**
	 * Returns the id that represents this term (or token) on the inverted index this accessor represents.
	 * If the term is not in the index, it returns -1 as the id.
	 * @param term
	 * @return
	 */
	public abstract int getTermId(String term);
	
	
	/**
	 * Returns the total number of words in this index.
	 * @return
	 */
	public abstract int getNoWords();
	
	/**
	 * Returns a list with the fields of the document
	 * @return
	 */
	public abstract Set<Field> getFields();
	
	public abstract Processor getDefaultProcessor();
}
