package com.ibibl.isabel.index;

public interface DocumentTermsIterator {

	public boolean hasNext();
	
	public int next();
	
}
