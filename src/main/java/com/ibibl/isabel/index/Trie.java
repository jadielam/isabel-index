package com.ibibl.isabel.index;

/**
 * A better implementation of a TrieNode
 * @author jadiel
 *
 */
public class Trie  {

	/**
	 * Counter that is used to assign an id to a Trie
	 */
	private static int WORD_COUNTER = 0;
	
	/**
	 * The word that this node represents, if any.
	 */
	private String word = null;
	
	/**
	 * Id of the word
	 */
	private int wordId;
	
	/**
	 * The children of this Trie.
	 */
	private TrieMap children = new TrieMap();
	
	/**
	 * Constructor
	 */
	public Trie(){
		
	}
	
	/**
	 * Returns the word id of the word inserted.
	 * @param word
	 * @return
	 */
	public int insert(String word){
		Trie node = this;
		
		for (int i = 0; i < word.length(); ++i){
			char a = word.charAt(i);
			if (!node.children.containsKey(a)){
				node.children.put(a, new Trie());
			}
			
			node = node.children.get(word.charAt(i));
		}
		
		if (null == node.word){
			node.word = word;
			node.wordId = Trie.WORD_COUNTER;
			Trie.WORD_COUNTER++;
		}
		
		return node.wordId;
	}
	
	/**
	 * Returns true if the tree rooted at this node contains that word.
	 * Otherwise it returns false.
	 * @param word
	 * @return
	 */
	public boolean contains(String word){
		if (null == word) return false;
		if (word.length()>0){
			if (word.length() == 1){
				Trie potentialCandidate = this.children.get(word.charAt(0));
				if (null != potentialCandidate.word) return true;
				else return false;
			}
			else{
				char nextChar = word.charAt(0);
				Trie nextStop = this.children.get(nextChar);
				if (null != nextStop){
					return nextStop.contains(word.substring(1));
				}
				return false;
			}
		}
		else return false;
	}
	
	/**
	 * Returns the id of the word that we are looking for.
	 * If the word is not in the trie, it returns -1;
	 * @param word
	 * @return
	 */
	public int getWordId(String word){
		if (null == word) return -1;
		if (word.length()>0){
			if (word.length() == 1){
				Trie potentialCandidate = this.children.get(word.charAt(0));
				if (null != potentialCandidate.word) return potentialCandidate.wordId;
				else return -1;
			}
			else{
				char nextChar = word.charAt(0);
				Trie nextStop = this.children.get(nextChar);
				if (null != nextStop){
					return nextStop.getWordId(word.substring(1));
				}
				return -1;
			}
		}
		else return -1;
	}
}
