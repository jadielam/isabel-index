// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.isabel.index;

/**
 * @author jdearmas
 * Memory efficient implementatino of a map that will
 * use char as key and TrieNode as value.
 * @since 1.0
 */
public class TrieMap {

	/**
	 * Contains the the values of the map
	 */
	private Trie [] nodes = new Trie[3];
	
	/**
	 * Contains the keys of the map
	 */
	private char [] charsIndex = new char[3];
	
	/**
	 * The next place of the array where we will insert
	 * the next values
	 */
	private char nextSpace = 0;
	
	/**
	 * Default constructor
	 */
	public TrieMap(){
		
	}
	
	/**
	 * Returns true if the key is in the map, false otherwise
	 * @param key the key
	 * @return true or false
	 */
	public boolean containsKey(char key){
		for (int i = 0; i < this.charsIndex.length; ++i){
			if (this.charsIndex[i] == key){
				return true;
			}
		}
		return false;
	}
	
	/**
	 * Puts the key value pair into the map
	 * @param key the character key
	 * @param node the node corresponding to that key
	 */
	public void put(char key, Trie node){
		if (this.nextSpace >= this.nodes.length){
			createMoreRoom();
		}
		this.nodes[this.nextSpace]=node;
		this.charsIndex[this.nextSpace]=key;
		
		this.nextSpace++;
	}
	
	/**
	 * Copies the current arrays to new arrays that are bigger
	 */
	private void createMoreRoom(){
		
		//1. Double the size of charsIndex and nodes
		Trie [] newNodes = new Trie[this.nodes.length + 2];
		char [] newCharsIndex = new char[this.charsIndex.length + 2];
		
		//2. Copy the oldOnes to the new ones.
		//Done this way to make it faster (loop unrolling)
		newNodes[0]=this.nodes[0];
		newNodes[1]=this.nodes[1];
		newNodes[2]=this.nodes[2];
		newCharsIndex[0]=this.charsIndex[0];
		newCharsIndex[1]=this.charsIndex[1];
		newCharsIndex[2]=this.charsIndex[2];
		for (int i = 3; i < this.nodes.length; ++i){
			newNodes[i] = this.nodes[i];
			newCharsIndex[i] = this.charsIndex[i];
		}
		
		this.nodes = newNodes;
		this.charsIndex = newCharsIndex;
	}
	
	/**
	 * Returns the TrieNode corresponding to that key
	 * If it is not in the map, it returns null
	 * @param key the key we are searching for.
	 * @return the Node or null if that key is not found.
	 */
	public Trie get(char key){
		for (int i = 0; i < this.charsIndex.length; ++i){
			if (this.charsIndex[i] == key){
				return this.nodes[i];
			}
		}
		return null;
	}
	
	/**
	 * Returns an array with the pairs of keys - nodes.
	 * Remember that the keys are characters.
	 * @return the array.
	 */
	public TriePair [] entries(){
		
		int numberOfEntries = 0;
		for (int i = 0; i < this.nodes.length; ++i){
			if (null == this.nodes[i]){
				break;
			}
			numberOfEntries++;
		}
		
		TriePair [] pairs = new TriePair[numberOfEntries];
		for (int i = 0; i < numberOfEntries; ++i){
			pairs[i] = new TriePair(this.charsIndex[i], this.nodes[i]);
		}
			
		return pairs;
	}
}

