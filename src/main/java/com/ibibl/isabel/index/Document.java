package com.ibibl.isabel.index;

/**
 * Accesses the ordered sequence of word ids that form
 * a document
 * @author jadiel
 *
 */
public interface Document {
	
	/**
	 * Returns the number of word ids of this document
	 * @return
	 */
	public int size();
	
	/**
	 * Returns the word id at position i
	 * @param i
	 * @return
	 */
	public int getWordIdAtPosition(int i);
	
	/**
	 * Returns the id of the document
	 * @param i
	 * @return
	 */
	public int getDocId();
}
