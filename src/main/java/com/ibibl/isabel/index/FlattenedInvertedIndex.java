package com.ibibl.isabel.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.NoSuchElementException;
import java.util.Set;

import com.ibibl.isabel.processor.Processor;
import com.ibibl.isabel.processor.SimpleTokenizerProcessor;

public class FlattenedInvertedIndex implements InvertedIndex {

	/**
	 * The root node to the trie datastructure that contains all the words
	 * together with the id of that word.
	 */
	private final Map<String, Integer> wordIds;
	
	/**
	 * Contains the the inverted indexes for each of the fields
	 * The fields are identified by a name.
	 */
	private final Map<Field, int []> invertedIndexes;
	
	/**
	 * For each field, it contains a map from wordId to wordPosition in 
	 * the inverted index for that field.
	 */
	private final Map<Field, Map<Integer, Integer>> wordPositions;
	
	/**
	 * Containes the direct indexes for each document for a given field.
	 * A direct index is simply a collection of documents and the sequence 
	 * of term ids that appear in those documents in order.
	 */
	private final Map<Field, int []> directIndexes;
	
	/**
	 * FOr each field it contains a map from docId to docPosition in
	 * the direct index.
	 */
	private final Map<Field, Map<Integer, Integer>> docPositions;
	
	private int wordCount = 0;
	
	/**
	 * Processor used to analyze each of the fields in the inverted index.
	 */
	private final Map<Field, Processor> processors;
	
	private final Processor defaultProcessor;
	
	public FlattenedInvertedIndex(){
		this.wordIds = new HashMap<>();
		this.invertedIndexes = new HashMap<>();
		this.wordPositions = new HashMap<>();
		this.directIndexes = new HashMap<>();
		this.docPositions = new HashMap<>();
		this.processors = new HashMap<>();
		this.defaultProcessor = new SimpleTokenizerProcessor();
	}
	
	/**
	 * @see com.ibibl.isabel.index.InvertedIndex#getWordSet()
	 */
	@Override
	public Set<String> getWordSet() {
		Set<String> toReturn = Collections.unmodifiableSet(this.wordIds.keySet());
		return toReturn;
	}
	
	/** 
	 * (non-Javadoc)
	 * @see com.ibibl.isabel.index.InvertedIndex#addCollection(java.lang.String, java.util.Set)
	 */
	@Override
	public void addField(Field field, Set<Pair<Integer, String>> documents){
		
		//1. Parse the documents and place the tokens in a map of token ids and also keep the sequence
		//of doc ids for each document temporarily saved.
		Map<Integer, int []> documentSequences = new HashMap<>();
		for (Pair<Integer, String> document : documents){
			int docId = document.getFirst();
			String docText = document.getSecond();
			String [] tokens = this.defaultProcessor.getTokens(docText);
			int [] tokenIds = new int[tokens.length];
			
			for (int i = 0; i < tokens.length; i++){
				int tokenId = insertTokenToMap(tokens[i]);
				tokenIds[i] = tokenId;
			}
			
			documentSequences.put(docId, tokenIds);
		}
		
		//2. Creating direct index
		this.createDirectIndex(field, documentSequences);
		
		//3. Creating inverted index
		this.createInvertedIndex(field, documents);

	}
	
	/**
	 * Inserts the token to this.wordIds. 
	 * If the token is already there it just returns the id.
	 * If not, it creates a next new id for it and it returns that new id.
	 * @param token
	 * @return
	 */
	private int insertTokenToMap(String token){
		Integer id = this.wordIds.get(token);
		if (id != null) return id;
		else{
			int wordId = this.wordCount;
			this.wordIds.put(token, wordId);
			this.wordCount++;
			return wordId;
		}
	}
	
	private void createInvertedIndex(Field field, Set<Pair<Integer, String>> documents){
		
		//0. Temporary storage
		Map<String, List<Pair<Integer, List<Integer>>>> tempInvertedIndex = new HashMap<>();
		
		//1. Populate temporary inverted index
		for (Pair<Integer, String> document : documents){
			int docId = document.getFirst();
			String docText = document.getSecond();
			String [] tokens = this.defaultProcessor.getTokens(docText);
			
			
			int positionCounter = 0;
			
			//1.1 Contains the positions of a word in a document. A word can have more than one position.
			//When creating the multi index, 1.1 goes inside another loop.
			Map<String, List<Integer>> wordPositions = new HashMap<String, List<Integer>>();
			for (String token : tokens){
				
				if (wordPositions.containsKey(token)){
					List<Integer> positions = wordPositions.get(token);
					positions.add(positionCounter);
				}
				else {
					List<Integer> positions = new ArrayList<Integer>();
					positions.add(positionCounter);
					wordPositions.put(token, positions);
				}
				positionCounter++;
			}
			
			//Here we add the processed document to the structures that hold the temporary
			//information that will later build the inverted index.
			Set<Entry<String, List<Integer>>> entrySet = wordPositions.entrySet();
			for (Entry<String, List<Integer>> e : entrySet){
				String token = e.getKey();
				List<Integer> positions = e.getValue();
				Pair<Integer, List<Integer>> docPositions = new Pair<>(docId, positions);
				if (tempInvertedIndex.containsKey(token)){
					List<Pair<Integer, List<Integer>>> tempPositions = tempInvertedIndex.get(token);
					tempPositions.add(docPositions);
				}
				else{
					List<Pair<Integer, List<Integer>>> tempPositions = new ArrayList<Pair<Integer, List<Integer>>>();
					tempPositions.add(docPositions);
					tempInvertedIndex.put(token, tempPositions);
				}
			}
		}
		
		//2. Make actual inverted index out of temporary inverted index.
		//2.0 Initializations
		Map<Integer, Integer> wordid_to_wordposition = new HashMap<Integer, Integer>();
		
		//2.1 Calculate the size of the actual inverted index.
		int invertedIndexSize = 0;
		invertedIndexSize += 1; 	//Field with the number of words
		Set<Entry<String, List<Pair<Integer, List<Integer>>>>> entrySet = tempInvertedIndex.entrySet();
		for (Entry<String, List<Pair<Integer, List<Integer>>>> e : entrySet){
			invertedIndexSize += 1;		//wordId
			invertedIndexSize += 1;		//Field with number of documents
			List<Pair<Integer, List<Integer>>> documentsPositions = e.getValue();
			for (Pair<Integer, List<Integer>> document : documentsPositions){
				invertedIndexSize += 1;  //docId
				invertedIndexSize += 1; //numberOfPositions
				
				List<Integer> positions = document.getSecond();
				int numberOfPositions = positions.size();
				invertedIndexSize += numberOfPositions; 	//one entry for each position.
			}
		}
		
		//2.2 Allocate the int[] array for that number of positions and build the index.
		int [] index = new int[invertedIndexSize];
		
		//next position in the index
		int next = 0; 			
		//2.2.1 Add number of words
		index[next] = tempInvertedIndex.size();
		next++;
		for (Entry<String, List<Pair<Integer, List<Integer>>>> e : entrySet){
			String token = e.getKey();
			
			//Add word id
			int wordId = this.getWordId(token);
			
			index[next] = wordId;
			wordid_to_wordposition.put(wordId, next);
			next++;
			
			//Add number of documents
			List<Pair<Integer, List<Integer>>> docs = e.getValue();
			int noDocs = docs.size();
			index[next] = noDocs;
			next++;
			
			//For each document
			for (Pair<Integer, List<Integer>> document : docs){
				//add doc id
				int docId = document.getFirst();
				index[next] = docId;
				next++;
				
				//add number of positions
				List<Integer> positions = document.getSecond();
				int noPositions = positions.size();
				index[next] = noPositions;
				next++;
				
				//add each position
				for (Integer i : positions){
					index[next] = i;
					next++;
				}
			}
		}
		
		this.invertedIndexes.put(field, index);
		this.wordPositions.put(field, wordid_to_wordposition);

	}
	
	/**
	 * 
	 * @param collectionName the name of the collection
	 * @param collectionDocuments the key of the map is the document id.
	 * The value of the map is an array that holds the id of the terms
	 * in the order that they appear in the document.
	 */
	private void createDirectIndex(Field field, Map<Integer, int []> collectionDocuments){
		
		int indexSize = 0;
		
		//The field to save the number of docs
		indexSize += 1; 						
		
		Set<Entry<Integer, int[]>> entrySet = collectionDocuments.entrySet();
		for (Entry<Integer, int []> entry : entrySet){
			indexSize += 1;						//The field indicating the document id
			indexSize += 1;						//The field indicating the number of terms
			int [] termIds = entry.getValue();
			indexSize += termIds.length;		//the fields holding the values. 		
		}
		
		//Allocating the direct index
		int [] directIndex = new int[indexSize];
		Map<Integer, Integer> docPositions = new HashMap<>();
		
		//Placing values in the direct index
		int next = 0;
		directIndex[next] = entrySet.size();		//The field with the number of docs
		next++;
		
		for (Entry<Integer, int []> entry : entrySet){
			int docId = entry.getKey();
			directIndex[next] = docId;
			docPositions.put(docId, next);
			next++;
			
			int [] termIds = entry.getValue();
			directIndex[next] = termIds.length;
			next++;
			
			for (int termId : termIds){
				directIndex[next] = termId;
				next++;
			}
		}	
		
		this.directIndexes.put(field, directIndex);
		this.docPositions.put(field, docPositions);
	}
	
	
	/** 
	 * @see com.ibibl.isabel.index.InvertedIndex#getProcessor()
	 */
	@Override
	public Processor getDefaultProcessor(){
		return this.defaultProcessor;
	}
	
	/**
	 * 
	 * @see com.ibibl.isabel.index.InvertedIndex#getWordId(java.lang.String)
	 */
	@Override
	public int getWordId(String word){
		Integer value = this.wordIds.get(word);
		if (null == value) return -1;
		return value;
	}
	
	@Override
	public IndexAccessor getIndexAccessor(){
		return new FixedInvertedIndexAccessor(this.invertedIndexes,
				this.wordPositions, this.wordIds, this.defaultProcessor,
				this.directIndexes, this.docPositions, this.processors);
	}
	
	/**
	 * This class knows how to  handle and access a FlattenedInvertedIndex.
	 * @author jadiel
	 *
	 */
	private static final class FixedInvertedIndexAccessor extends IndexAccessor{
		
		private final Map<Field, int[]> invertedIndexes;
		private final Map<Field, Map<Integer, Integer>> wordPositions;
		private final Map<String, Integer> wordIds;
		private final Map<Field, Processor> processors;
		private final Processor defaultProcessor;
		private final Map<Field, int[]> directIndexes;
		private final Map<Field, Map<Integer, Integer>> docsPositions;
		private final int numberOfWords;
		
		FixedInvertedIndexAccessor(Map<Field, int[]> invertedIndexes,
				Map<Field, Map<Integer, Integer>> wordPositionsIndex,
				Map<String, Integer> wordIds, Processor processor,
				Map<Field, int []> directIndexes, Map<Field, Map<Integer, Integer>> docsPositions,
				Map<Field, Processor> processors){
			
			this.invertedIndexes = invertedIndexes;
			this.wordPositions = wordPositionsIndex;
			this.wordIds = wordIds;
			this.defaultProcessor = processor;
			this.directIndexes = directIndexes;
			this.docsPositions = docsPositions;
			this.processors = processors;
			
			if (null == this.invertedIndexes || null == this.wordPositions
					|| null == this.wordIds || null == this.defaultProcessor
					|| null == this.processors
					|| null == this.directIndexes || null == this.docsPositions){
				throw new IllegalStateException();
			}else{
				this.numberOfWords = this.wordIds.size();
			}
		}

		@Override
		public Processor getDefaultProcessor(){
			return this.defaultProcessor;
		}
		
		@Override
		public int getTermId(String term) {
			Integer id = this.wordIds.get(term);
			if (null == id) return -1;
			return id;
		}

		@Override
		public FieldAccessor getFieldAccessor(Field field) {
		
			Map<Integer, Integer> wordPositions = this.wordPositions.get(field);
			Map<Integer, Integer> docsPositions = this.docsPositions.get(field);
			int [] invertedIndex = this.invertedIndexes.get(field);
			int [] directIndex = this.directIndexes.get(field);
			Processor processor = this.processors.get(field);
			if (null == processor) processor = this.defaultProcessor;
			
			return new FlattenedFieldAccessor(this.wordIds,
					wordPositions,
					docsPositions,
					invertedIndex,
					directIndex,
					processor);
			
		}

		@Override
		public Set<Field> getFields() {
			return this.invertedIndexes.keySet();
		}


		@Override
		public int getNoWords(){
			return this.numberOfWords;
		}

		private static final class FlattenedFieldAccessor extends FieldAccessor {

			private final Map<String, Integer> wordIds;
			private final Map<Integer, Integer> wordPositions;
			private final Map<Integer, Integer> docsPositions;
			private final int [] invertedIndex;
			private final int [] directIndex;
			private final Processor processor;
			private final int numberOfDocuments;
			private final int numberOfWords;
			
			public FlattenedFieldAccessor(Map<String, Integer> wordIds,
					Map<Integer, Integer> wordPositions,
					Map<Integer, Integer> docsPositions,
					int [] invertedIndex,
					int [] directIndex,
					Processor processor){
				
				this.wordIds = wordIds;
				this.wordPositions = wordPositions;
				this.docsPositions = docsPositions;
				this.invertedIndex = invertedIndex;
				this.directIndex = directIndex;
				this.processor = processor;
				
				if (null == this.invertedIndex 
					|| null == this.directIndex
					|| null == this.processor
					|| null == this.docsPositions
					|| null == this.wordPositions){
					throw new IllegalStateException();
				}
				this.numberOfDocuments = this.directIndex.length == 0 ? 0 : this.directIndex[0];
				this.numberOfWords = this.invertedIndex.length == 0 ? 0 : this.invertedIndex[0];
			}
			 
			
			@Override
			public DocumentTermsCollection getDocumentTermsCollection(String term) {
				
				Integer wordId = this.wordIds.get(term);
				if (null == wordId) return null;
				int wordPosition = this.wordPositions.get(wordId);
				return new FlattenedIndexDocumentTermsCollection(this.invertedIndex, wordPosition);
			}

			@Override
			public DocumentTermsCollection getDocumentTermsCollection(Integer termId) {
				Integer wordPosition = this.wordPositions.get(termId);
				if (null == wordPosition) return null;
				return new FlattenedIndexDocumentTermsCollection(this.invertedIndex, wordPosition);
			}
			
			@Override
			public Processor getProcessor() {
				return this.processor;
			}

			@Override
			public Document getDocument(int docId) {
				Integer docPosition = this.docsPositions.get(docId);
				if (null == docPosition) return null;
				return new FlattenedDocument(this.directIndex, docPosition);
			}
			
			@Override
			public int getNoDocuments() {
				return this.numberOfDocuments;
			}
			
			@Override
			public int getNoWords(){
				return this.numberOfWords;
			}

			@Override
			public Processor getProcessor(String field) {
				return this.processor;
			}	
		}
		
		private static final class FlattenedDocument implements Document {

			private final int [] directIndex;
			private final int docPosition;
			private final int size;
			
			public FlattenedDocument(int [] directIndex, int docPosition){
				this.directIndex = directIndex;
				this.docPosition = docPosition;
				this.size = this.directIndex[docPosition + 1];
			}
			
			@Override
			public int size() {
				return this.size;
			}

			@Override
			public int getWordIdAtPosition(int i) {
				if (i >= this.size){
					throw new IndexOutOfBoundsException();
				}
				return this.directIndex[docPosition + 2 + i];
			}

			@Override
			public int getDocId() {
				return this.directIndex[this.docPosition];
			}
			
		}
		private static final class FlattenedIndexDocumentTermsCollection extends DocumentTermsCollection {

			private final int [] invertedIndex;
			
			/**
			 * THe position of the word in the index
			 */
			private int wordPosition;
			private int numberOfDocuments;
			
			FlattenedIndexDocumentTermsCollection(int [] invertedIndex,
					int wordPosition){
				
				this.invertedIndex = invertedIndex;
				this.wordPosition = wordPosition;
				this.numberOfDocuments = this.invertedIndex[this.wordPosition+1];
			}
			
			public int getWordId(){
				return this.invertedIndex[wordPosition];
			}
			
			public int size(){
				return this.numberOfDocuments;
			}
			
			@Override
			public Iterator<DocumentTerms> iterator() {
				return new FlattenedIndexDocumentTermsIterator(this.invertedIndex,
						this.wordPosition);
			}
			
			private static final class FlattenedIndexDocumentTermsIterator implements Iterator<DocumentTerms> {
				
				/**
				 * The current position of the iterator
				 */
				private int currentDocument = -1;
				private int currentDocumentPosition;
				private final int numberOfDocuments;
				
				/**
				 * The position of the word in the index
				 */
				private final int wordPosition; 
				private final int [] invertedIndex;
				
				FlattenedIndexDocumentTermsIterator(int [] invertedIndex, int wordPosition){
					this.invertedIndex = invertedIndex;
					this.wordPosition = wordPosition;
					this.numberOfDocuments = this.invertedIndex[this.wordPosition+1];
					this.currentDocumentPosition = (this.numberOfDocuments > 0) ? this.wordPosition + 2 : this.wordPosition+1;
				}
				
				@Override
				public boolean hasNext() {
					if (this.currentDocument < (numberOfDocuments - 1)) return true;
					return false;
					
				}

				@Override
				public DocumentTerms next() {
					if (!this.hasNext()){
						throw new NoSuchElementException();
					}
					this.currentDocument++;
					int tempStart = this.currentDocumentPosition + 2;
					int tempEnd = this.currentDocumentPosition + 2 + this.invertedIndex[this.currentDocumentPosition+1];
					this.currentDocumentPosition = tempEnd;
					return new FlattenedIndexDocumentTerms(this.invertedIndex, tempStart, tempEnd);
					
				}
				
			}
			
			private static final class FlattenedIndexDocumentTerms implements DocumentTerms {

				private final int [] invertedIndex;
				
				/**
				 * Marks the beginning of the terms, not of the document, inclusive
				 */
				private final int begin;
				
				/**
				 * Marks the end of the terms of the document, exclusive.
				 */
				private final int end;
				private final int size;
				
				/**
				 * 
				 * @param invertedIndex
				 * @param begin It is the place in invertedIndex where the Document Temrs begin, inclusive
				 * @param end It is the position in invertedIndex where the document terms end, exclusive.
				 */
				FlattenedIndexDocumentTerms(int [] invertedIndex,
						int begin, int end){
					
					this.invertedIndex = invertedIndex;
					this.begin = begin;
					this.end = end;
					this.size = Math.max(end - begin, 0);
				}
				
				@Override
				public DocumentTermsIterator iterator() {
					
					return new DocumentTermsIteratorFlattenedIndex(this.invertedIndex,
							this.begin, this.end);
				}

				@Override
				public int get(int i) {
					int position = begin + i;
					if (position >= end) throw new ArrayIndexOutOfBoundsException();
					return this.invertedIndex[position];
				}
				
				@Override
				public int getDocumentId(){
					return this.invertedIndex[this.begin-2];
				}

				@Override
				public int size() {
					
					return this.size;
				}
				
				private static final class DocumentTermsIteratorFlattenedIndex implements DocumentTermsIterator{

					private int currentPosition = 0;
					private final int [] invertedIndex;
					private final int begin;
					private final int end;
					
					DocumentTermsIteratorFlattenedIndex(int [] invertedIndex, int begin, int end){
						this.invertedIndex = invertedIndex;
						this.begin = begin;
						this.end = end;
					}
					
					@Override
					public boolean hasNext() {
						if (this.begin + this.currentPosition < this.end) return false;
						return true;
					}

					@Override
					public int next() {
						return this.invertedIndex[this.currentPosition++];
					}
					
				}
				
			}
								
		}




	}



}
