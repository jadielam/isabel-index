package com.ibibl.isabel.index;

import java.util.List;
import java.util.Set;

import com.ibibl.isabel.processor.Processor;

/**
 * Inverted index that is used for collections
 * that have documents with multiple versions, 
 * such as the BIble
 * @author jadiel
 *
 */
public interface MultiInvertedIndex {

	/**
	 * Adds the collection to the index
	 * @param collectionName
	 * @param documents A list of pairs, with the integer being the documentId and the String being
	 * the text of that document
	 */
	public abstract void addCollection(String collectionName,
			Set<Pair<Integer, List<String>>> documents);

	/**
	 * Returns the processor of the text that was used to create the index.
	 * @return
	 */
	public abstract Processor getProcessor();

	/**
	 * Returns the wordId for the given word. If not found, it returns - 1
	 * @param word
	 * @return
	 */
	public abstract int getWordId(String word);

	/**
	 * Returns the IndexAccessor object for the particular collection
	 * 
	 * @param collection
	 * @return
	 */
	public abstract IndexAccessor getIndexAccessor(String collection);
	
	/**
	 * Returns all the words that the index contains as an unmodifiable set.
	 * @return
	 */
	public abstract Set<String> getWordSet();

	
}
