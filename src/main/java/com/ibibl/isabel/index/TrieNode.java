// **************************************************
//
// Modification Tracking:
// $Revision:$
// $LastChangedBy:$
// $LastChangedDate:$
//
// **************************************************
package com.ibibl.isabel.index;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;


/**
 * @author jdearmas
 * Trie datastructure to be used as an indexing structure of strings.
 * 
 * @since 1.0 
 */
public class TrieNode {

	/**
	 * The word that this node represents (if some word in the dictionary ends
	 * at this node.  If not, then null.
	 */
	private String word = null;
	
	/**
	 * The key is the document collection to which the list of document entries belongs to. 
	 */
	private Map<String, List<DocumentEntry>> documentsIndex = new HashMap<String, List<DocumentEntry>>();
	
	/**
	 * The child nodes of this node.
	 */
	private TrieNodeMap children = new TrieNodeMap();
	
	/**
	 * Default constructor.
	 */
	public TrieNode(){
		
	}

	/**
	 * 
	 * @param word The word being inserted to the index
	 * @param documentId the document id to which this word belongs to.
	 * @param positions The positions in that document where this word belongs, starting at positon 0.
	 */
	public void insert(String word, long documentId, int [] positions, String collectionName){
		TrieNode node = this;
		
		for (int i = 0; i < word.length(); ++i){
			char a = word.charAt(i);
			if (!node.children.containsKey(a)){
				node.children.put(a, new TrieNode());
			}
			
			node = node.children.get(word.charAt(i));
		}
		
		if (null == node.word){
			node.word = word;
		}
		if (!node.documentsIndex.containsKey(collectionName)){
			node.documentsIndex.put(collectionName, new ArrayList<DocumentEntry>());
		}
		List<DocumentEntry> entries = node.documentsIndex.get(collectionName);
		DocumentEntry entry = new DocumentEntry(documentId, positions);
		entries.add(entry);
	}
	
	
	/**
	 * Deletes a word from the trie.
	 * @param aWord The word to be deleted.
	 * @throws UnsupportedOperationException whenever the method is call
	 * because the method is not yet supported.
	 */
	public void delete(String aWord){
		throw new UnsupportedOperationException();
	}
	
	/**
	 * Returns all the children node of this trie.
	 * @return the children
	 */
	public TrieNodeMap getChildren(){
		return this.children;
	}
	
	/**
	 * Returns the word corresponding to this node
	 * @return the String word.
	 */
	public String getWord(){
		return this.word;
	}
		
	public List<DocumentEntry> getDocumentEntries(String collection){
		return this.documentsIndex.get(collection);
	}

}
