package com.ibibl.isabel.index;

import java.util.Set;

import com.ibibl.isabel.processor.Processor;

public interface InvertedIndex {

	/**
	 * Adds the collection to the index
	 * @param collectionName
	 * @param documents A list of pairs, with the integer being the documentId and the String being
	 * the text of that document
	 */
	public abstract void addField(Field field,
			Set<Pair<Integer, String>> documents);

	/**
	 * Returns the processor of the text that was used to create the index.
	 * @return
	 */
	public abstract Processor getDefaultProcessor();

	/**
	 * Returns the wordId for the given word. If not found, it returns - 1
	 * @param word
	 * @return
	 */
	public abstract int getWordId(String word);

	/**
	 * Returns the IndexAccessor object for the particular field
	 * 
	 * @param collection
	 * @return
	 */
	public abstract IndexAccessor getIndexAccessor();
	
	/**
	 * Returns all the words that the index contains as an unmodifiable set.
	 * @return
	 */
	public abstract Set<String> getWordSet();

}