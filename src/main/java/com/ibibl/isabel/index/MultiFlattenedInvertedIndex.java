package com.ibibl.isabel.index;

import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.NoSuchElementException;
import java.util.Set;
import java.util.Map.Entry;

import com.ibibl.isabel.processor.Processor;
import com.ibibl.isabel.processor.SimpleTokenizerProcessor;

/**
 * The multi index version of the 
 * flattened inverted index.
 * @author jadiel
 *
 */
public class MultiFlattenedInvertedIndex implements MultiInvertedIndex {

	/**
	 * The root node to the trie datastructure that contains all the words
	 * together with the id of that word.
	 */
	private final Map<String, Integer> wordIds;
	
	/**
	 * Contains the the inverted indexes for each of the collections
	 * The collections are identified by a name.
	 */
	private final Map<String, int []> invertedIndexes;
	
	/**
	 * For each collection, it contains a map from wordId to wordPosition in 
	 * the inverted index for that collection.
	 */
	private final Map<String, Map<Integer, Integer>> wordPositions;
	
	/**
	 * FOr each collection it contains a map from docId to docPosition in
	 * the direct index.
	 */
	private final Map<String, Map<Integer, Integer>> docPositions;
	
	private int wordCount = 0;
	
	/**
	 * Processor used to tokenize the documents inserted to the index.
	 */
	private Processor processor;
	
	public MultiFlattenedInvertedIndex(){
		this.wordIds = new HashMap<>();
		this.invertedIndexes = new HashMap<>();
		this.wordPositions = new HashMap<>();
		this.docPositions = new HashMap<>();
		this.processor = new SimpleTokenizerProcessor();
	}
	
	/**
	 * @see com.ibibl.isabel.index.InvertedIndex#getWordSet()
	 */
	@Override
	public Set<String> getWordSet() {
		Set<String> toReturn = Collections.unmodifiableSet(this.wordIds.keySet());
		return toReturn;
	}
	
	/** 
	 * (non-Javadoc)
	 * @see com.ibibl.isabel.index.InvertedIndex#addCollection(java.lang.String, java.util.Set)
	 */
	@Override
	public void addCollection(String collectionName, Set<Pair<Integer, List<String>>> documents){
		
		//3. Creating inverted index
		this.createInvertedIndex(collectionName, documents);

	}
	
	/**
	 * Inserts the token to this.wordIds. 
	 * If the token is already there it just returns the id.
	 * If not, it creates a next new id for it and it returns that new id.
	 * @param token
	 * @return
	 */
	private int insertTokenToMap(String token){
		Integer id = this.wordIds.get(token);
		if (id != null) return id;
		else{
			int wordId = this.wordCount;
			this.wordIds.put(token, wordId);
			this.wordCount++;
			return wordId;
		}
	}
	
	private void createInvertedIndex(String collectionName, Set<Pair<Integer, List<String>>> documents){
		
		//0. Temporary storage
		Map<String, List<Pair<Integer, List<Integer>>>> tempInvertedIndex = new HashMap<>();
		
		//1. Populate temporary inverted index
		for (Pair<Integer, List<String>> document : documents){
			int docId = document.getFirst();
			
			List<String> documentVersions = document.getSecond();
			Map<String, List<Integer>> wordPositions = new HashMap<String, List<Integer>>();
			
			for (String docText : documentVersions){
				String [] tokens = processor.getTokens(docText);
				int positionCounter = 0;
				
				//1.1 Contains the positions of a word in a document. A word can have more than one position.
				//When creating the multi index, 1.1 goes inside another loop.
				
				for (String token : tokens){
					
					if (wordPositions.containsKey(token)){
						List<Integer> positions = wordPositions.get(token);
						positions.add(positionCounter);
					}
					else {
						List<Integer> positions = new ArrayList<Integer>();
						positions.add(positionCounter);
						wordPositions.put(token, positions);
					}
					positionCounter++;
				}
			}
			
			
			//Here we add the processed document to the structures that hold the temporary
			//information that will later build the inverted index.
			Set<Entry<String, List<Integer>>> entrySet = wordPositions.entrySet();
			for (Entry<String, List<Integer>> e : entrySet){
				String token = e.getKey();
				List<Integer> positions = e.getValue();
				Collections.sort(positions);
				
				Pair<Integer, List<Integer>> docPositions = new Pair<>(docId, positions);
				if (tempInvertedIndex.containsKey(token)){
					List<Pair<Integer, List<Integer>>> tempPositions = tempInvertedIndex.get(token);
					tempPositions.add(docPositions);
				}
				else{
					List<Pair<Integer, List<Integer>>> tempPositions = new ArrayList<Pair<Integer, List<Integer>>>();
					tempPositions.add(docPositions);
					tempInvertedIndex.put(token, tempPositions);
				}
			}
		}
		
		//2. Make actual inverted index out of temporary inverted index.
		//2.0 Initializations
		Map<Integer, Integer> wordid_to_wordposition = new HashMap<Integer, Integer>();
		
		//2.1 Calculate the size of the actual inverted index.
		int invertedIndexSize = 0;
		invertedIndexSize += 1; 	//Field with the number of words
		Set<Entry<String, List<Pair<Integer, List<Integer>>>>> entrySet = tempInvertedIndex.entrySet();
		for (Entry<String, List<Pair<Integer, List<Integer>>>> e : entrySet){
			invertedIndexSize += 1;		//wordId
			invertedIndexSize += 1;		//Field with number of documents
			List<Pair<Integer, List<Integer>>> documentsPositions = e.getValue();
			for (Pair<Integer, List<Integer>> document : documentsPositions){
				invertedIndexSize += 1;  //docId
				invertedIndexSize += 1; //numberOfPositions
				
				List<Integer> positions = document.getSecond();
				int numberOfPositions = positions.size();
				invertedIndexSize += numberOfPositions; 	//one entry for each position.
			}
		}
		
		//2.2 Allocate the int[] array for that number of positions and build the index.
		int [] index = new int[invertedIndexSize];
		
		//next position in the index
		int next = 0; 			
		//2.2.1 Add number of words
		index[next] = tempInvertedIndex.size();
		next++;
		for (Entry<String, List<Pair<Integer, List<Integer>>>> e : entrySet){
			String token = e.getKey();
			
			//Add word id
			int wordId = this.getWordId(token);
			
			index[next] = wordId;
			wordid_to_wordposition.put(wordId, next);
			next++;
			
			//Add number of documents
			List<Pair<Integer, List<Integer>>> docs = e.getValue();
			int noDocs = docs.size();
			index[next] = noDocs;
			next++;
			
			//For each document
			for (Pair<Integer, List<Integer>> document : docs){
				//add doc id
				int docId = document.getFirst();
				index[next] = docId;
				next++;
				
				//add number of positions
				List<Integer> positions = document.getSecond();
				int noPositions = positions.size();
				index[next] = noPositions;
				next++;
				
				//add each position
				for (Integer i : positions){
					index[next] = i;
					next++;
				}
			}
		}
		
		this.invertedIndexes.put(collectionName, index);
		this.wordPositions.put(collectionName, wordid_to_wordposition);

	}
	
	
	/** 
	 * @see com.ibibl.isabel.index.InvertedIndex#getProcessor()
	 */
	@Override
	public Processor getProcessor(){
		return this.processor;
	}
	
	/**
	 * 
	 * @see com.ibibl.isabel.index.InvertedIndex#getWordId(java.lang.String)
	 */
	@Override
	public int getWordId(String word){
		Integer value = this.wordIds.get(word);
		if (null == value) return -1;
		return value;
	}
	
	@Override
	public IndexAccessor getIndexAccessor(String collection){
		return new FixedInvertedIndexAccessor(collection, this.invertedIndexes,
				this.wordPositions, this.wordIds, this.processor,
				this.docPositions);
	}
	
	/**
	 * This class knows how to  handle and access a FlattenedInvertedIndex.
	 * @author jadiel
	 *
	 */
	private static final class FixedInvertedIndexAccessor extends IndexAccessor{
		
		private final int [] invertedIndex;
		private final Map<Integer, Integer> wordPositions;
		private final Map<String, Integer> wordIds;
		private final Processor processor;
		private final Map<Integer, Integer> docsPositions;
		
		FixedInvertedIndexAccessor(String collection, Map<String, int[]> invertedIndexes,
				Map<String, Map<Integer, Integer>> wordPositionsIndex,
				Map<String, Integer> wordIds, Processor processor,
				Map<String, Map<Integer, Integer>> docsPositions){
			
			super(collection);
			this.invertedIndex = invertedIndexes.get(collection);
			this.wordPositions = wordPositionsIndex.get(collection);
			this.wordIds = wordIds;
			this.processor = processor;
			this.docsPositions = docsPositions.get(collection);
			
			if (null == this.invertedIndex || null == this.wordPositions
					|| null == this.wordIds || null == this.processor
					|| null == this.docsPositions){
				throw new IllegalStateException();
			}
		}

		@Override
		public DocumentTermsCollection getDocumentTermsCollection(String term) {
			
			Integer wordId = this.wordIds.get(term);
			if (null == wordId) return null;
			int wordPosition = this.wordPositions.get(wordId);
			return new FlattenedIndexDocumentTermsCollection(this.invertedIndex, wordPosition);
		}

		@Override
		public DocumentTermsCollection getDocumentTermsCollection(Integer termId) {
			Integer wordPosition = this.wordPositions.get(termId);
			if (null == wordPosition) return null;
			return new FlattenedIndexDocumentTermsCollection(this.invertedIndex, wordPosition);
		}
		
		@Override
		public Processor getProcessor() {
			return this.processor;
		}

		@Override
		public int getTermId(String term) {
			Integer id = this.wordIds.get(term);
			if (null == id) return -1;
			return id;
		}

		@Override
		public Document getDocument(int docId) {
			throw new UnsupportedOperationException();
		}

		private static final class FlattenedIndexDocumentTermsCollection extends DocumentTermsCollection {

			private final int [] invertedIndex;
			
			/**
			 * THe position of the word in the index
			 */
			private int wordPosition;
			
			FlattenedIndexDocumentTermsCollection(int [] invertedIndex,
					int wordPosition){
				
				this.invertedIndex = invertedIndex;
				this.wordPosition = wordPosition;
			}
			
			public int getWordId(){
				return this.invertedIndex[wordPosition];
			}
			@Override
			public Iterator<DocumentTerms> iterator() {
				return new FlattenedIndexDocumentTermsIterator(this.invertedIndex,
						this.wordPosition);
			}
			
			private static final class FlattenedIndexDocumentTermsIterator implements Iterator<DocumentTerms> {
				
				/**
				 * The current position of the iterator
				 */
				private int currentDocument = -1;
				private int currentDocumentPosition;
				private final int numberOfDocuments;
				
				/**
				 * The position of the word in the index
				 */
				private final int wordPosition; 
				private final int [] invertedIndex;
				
				FlattenedIndexDocumentTermsIterator(int [] invertedIndex, int wordPosition){
					this.invertedIndex = invertedIndex;
					this.wordPosition = wordPosition;
					this.numberOfDocuments = this.invertedIndex[this.wordPosition+1];
					this.currentDocumentPosition = (this.numberOfDocuments > 0) ? this.wordPosition + 2 : this.wordPosition+1;
				}
				
				@Override
				public boolean hasNext() {
					if (this.currentDocument < (numberOfDocuments - 1)) return true;
					return false;
					
				}

				@Override
				public DocumentTerms next() {
					if (!this.hasNext()){
						throw new NoSuchElementException();
					}
					this.currentDocument++;
					int tempStart = this.currentDocumentPosition + 2;
					int tempEnd = this.currentDocumentPosition + 2 + this.invertedIndex[this.currentDocumentPosition+1];
					this.currentDocumentPosition = tempEnd;
					return new FlattenedIndexDocumentTerms(this.invertedIndex, tempStart, tempEnd);
					
				}
				
			}
			
			private static final class FlattenedIndexDocumentTerms implements DocumentTerms {

				private final int [] invertedIndex;
				
				/**
				 * Marks the beginning of the terms, not of the document, inclusive
				 */
				private final int begin;
				
				/**
				 * Marks the end of the terms of the document, exclusive.
				 */
				private final int end;
				private final int size;
				
				/**
				 * 
				 * @param invertedIndex
				 * @param begin It is the place in invertedIndex where the Document Temrs begin, inclusive
				 * @param end It is the position in invertedIndex where the document terms end, exclusive.
				 */
				FlattenedIndexDocumentTerms(int [] invertedIndex,
						int begin, int end){
					
					this.invertedIndex = invertedIndex;
					this.begin = begin;
					this.end = end;
					this.size = Math.max(end - begin, 0);
				}
				
				@Override
				public DocumentTermsIterator iterator() {
					
					return new DocumentTermsIteratorFlattenedIndex(this.invertedIndex,
							this.begin, this.end);
				}

				@Override
				public int get(int i) {
					int position = begin + i;
					if (position >= end) throw new ArrayIndexOutOfBoundsException();
					return this.invertedIndex[position];
				}
				
				@Override
				public int getDocumentId(){
					return this.invertedIndex[this.begin-2];
				}

				@Override
				public int size() {
					
					return this.size;
				}
				
				private static final class DocumentTermsIteratorFlattenedIndex implements DocumentTermsIterator{

					private int currentPosition = 0;
					private final int [] invertedIndex;
					private final int begin;
					private final int end;
					
					DocumentTermsIteratorFlattenedIndex(int [] invertedIndex, int begin, int end){
						this.invertedIndex = invertedIndex;
						this.begin = begin;
						this.end = end;
					}
					
					@Override
					public boolean hasNext() {
						if (this.begin + this.currentPosition < this.end) return false;
						return true;
					}

					@Override
					public int next() {
						return this.invertedIndex[this.currentPosition++];
					}
					
				}
				
			}
								
		}




	}


}
