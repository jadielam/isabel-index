package com.ibibl.isabel.index;

import com.ibibl.isabel.processor.Processor;

public abstract class FieldAccessor {

	

	/**
	 * Returns the DocumentTermsCollection given the term
	 * Returns null if the given term does not exist.
	 * @param term
	 * @return
	 */
	public abstract DocumentTermsCollection getDocumentTermsCollection(String term);
	
	/**
	 * Returns the DocumentTermsCollection given the term id
	 * Returns null if the given term does not exists
	 * @param termId
	 * @return
	 */
	public abstract DocumentTermsCollection getDocumentTermsCollection(Integer termId);
	
	/**
	 * Returns the processor that was used to process the text that was entered to
	 * the inverted index.
	 * @return
	 */
	public abstract Processor getProcessor(String field);
	
	/**
	 * Returns the Document for the given docId. Returns null if the docId is not present.
	 * @param docId
	 * @return
	 */
	public abstract Document getDocument(int docId);

	/**
	 * Returns the total number of documents indexed at this field
	 * @return
	 */
	public abstract int getNoDocuments();
	
	/**
	 * Returns the total number of words indexed at this field.
	 * @return
	 */
	public abstract int getNoWords();

	/**
	 * Returns the processor for this field
	 * @return
	 */
	public abstract Processor getProcessor();
}
