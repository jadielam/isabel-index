package com.ibibl.isabel.index;

public class DocumentEntry {

	private final long docId;
	private final int [] positions;
	
	public DocumentEntry(long docId, int[] positions){
		this.docId = docId;
		this.positions = positions;
	}
	
	public long getDocId(){
		return this.docId;
	}
	
	public int [] getPositions(){
		return this.positions;
	}
}
