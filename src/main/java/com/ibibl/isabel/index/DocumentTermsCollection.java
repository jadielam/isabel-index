package com.ibibl.isabel.index;

/**
 * This class is a collection of DocumentTerms
 * @author jadiel
 *
 */
public abstract class DocumentTermsCollection implements Iterable<DocumentTerms> {

	/**
	 * Returns the number of documents in the collection
	 * @return
	 */
	public abstract int size();
}
