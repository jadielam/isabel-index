package com.ibibl.isabel.common;

import java.util.Collection;
import java.util.Comparator;

public class BucketSort {

	/**
	 * Does Bucket Sort of a collection with numeric value, it assumes uniform distribution of the values.
	 * @param elements
	 * @param comparator
	 * @param accessor
	 */
	public static <T, S extends Number> void sort(Collection<T> elements, Comparator<T> comparator, Accessor<T, S> accessor){
		
		float maximum = Float.MIN_VALUE;
		float minimum = Float.MAX_VALUE;
		
		for (T t : elements){
			S numberValue = accessor.getItem(t);
			float value = numberValue.floatValue();
			if (value > maximum) maximum = value;
			else if (value < minimum) minimum = value;
		}
		
		int range = (int)Math.ceil(Math.max(maximum - minimum, 0));
		int arraySize = elements.size()/5+1;
		//TODO: FInish this.
		
	}
}

interface Accessor<T, S extends Number>{
	
	public S getItem(T object);
	
}
