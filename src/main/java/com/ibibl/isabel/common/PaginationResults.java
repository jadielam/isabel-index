package com.ibibl.isabel.common;

public class PaginationResults {

	private final int totalResults;
	private final int startIndex;
	private final int [] resultsSubset;
	
	public PaginationResults (int totalResults, int startIndex,
			int [] resultsSubset){
		this.totalResults = totalResults;
		this.startIndex = startIndex;
		this.resultsSubset = resultsSubset;
	}

	public int getTotalResults() {
		return totalResults;
	}

	public int getStartIndex() {
		return startIndex;
	}

	public int[] getResultsSubset() {
		return resultsSubset;
	}

}
