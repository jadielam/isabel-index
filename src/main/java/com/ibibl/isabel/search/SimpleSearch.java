package com.ibibl.isabel.search;

import com.ibibl.isabel.index.TrieNode;
import com.ibibl.isabel.index.TrieNodeMap;

public class SimpleSearch implements Searcher {

	//TODO: Right now I am modifying search to adapt to my index.
	//BUt I need to make my index structure more memory efficient
	//and with less redundancy.
	@Override
	public TrieNode search(TrieNode index, String word) {
		return searchRecursive(index, word, word);	
	}
	
	private TrieNode searchRecursive(TrieNode index, String word, String suffix){
		if (null == word ) return null;
		
		String indexWord = index.getWord();
		if (indexWord != null){
			if (indexWord.equals(word)) {
				return index;
			}
			else{
				TrieNodeMap children = index.getChildren();
				char nextChar = suffix.charAt(0);
				if (children.containsKey(nextChar)){
					TrieNode newIndex = children.get(nextChar);
					
					return searchRecursive(newIndex, word, suffix.substring(1));
				}
				return null;
			}
		}
		
		else {
			//TODO; Clean this code when I get a time.
			//
			if (suffix.length() <= 0 ) return null;
			
			TrieNodeMap children = index.getChildren();
			char nextChar = suffix.charAt(0);
			
			if (children.containsKey(nextChar)){
				TrieNode newIndex = children.get(nextChar);
				
				return searchRecursive(newIndex, word, suffix.substring(1));
			}
			return null;
		}
		
	}

}
