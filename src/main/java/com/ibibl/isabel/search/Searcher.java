package com.ibibl.isabel.search;

import com.ibibl.isabel.index.TrieNode;

public interface Searcher {

	/**
	 * Searches for word word in TrieNode index and returns the node that
	 * contains it.  Returns null if the node is not in the index
	 * @param index
	 * @param word
	 * @return
	 */
	public TrieNode search(TrieNode index, String word);
}
