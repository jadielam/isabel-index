package com.ibibl.isabel.search;

import java.util.List;

import com.ibibl.isabel.index.TrieNode;

public interface SimilaritySearcher {

	/**
	 * Searches for word word in TrieNode index and returns list of nodes that 
	 * are at a distance d or less of the given node.
	 * @param index
	 * @param word
	 * @return
	 */
	public List<TrieNode> search(TrieNode index, String word, int distance);
}
